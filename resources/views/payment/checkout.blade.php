<!DOCTYPE html>
<html>
<head>
    @include('head')
</head>
@include('navbar')
<body class="bg">
<div class="container">
@if($listing)
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row interview-row">
                <div class="interviewer-img col-xs-2">
                    <img src="{{URL::asset('files/profile_images/')}}/{{$listing['interviewer']['user']['profile_image_url']}}">
                </div>
                <div class="interview-details col-xs-10">
                    <div class="row">
                        <div class="interviewer-name"><a class="black-link" href="interviewer/profile/{{$listing['interviewer_id']}}">{{$listing['interviewer']['user']['first_name']}} {{$listing['interviewer']['user']['last_name']}}</a> @if($listing['interviewer']['verified'] == 1)<i class="fa fa-check-circle" data-toggle="tooltip" data-placement="bottom" title="Certified Interviewer"></i> @endif </div>
                        <div class="star-rating">
                            <?php $r=0 ?>
                            @for($i=1;$i<=intval($listing['interviewer']['rating']);$i++)
                                <span class="fa fa-star" data-rating="{{$i}}"></span>
                            @endfor
                            @if(($listing['interviewer']['rating'] - intval($listing['interviewer']['rating']))>=0.25 && ($listing['interviewer']['rating'] - intval($listing['interviewer']['rating']))<=0.75)
                                <span class="fa fa-star-half-o" data-rating="2"></span>
                                <?php $r=1 ?>
                            @endif
                            @for($i=intval($listing['interviewer']['rating'])+$r;$i<5;$i++)
                                <span class="fa fa-star-o" data-rating="{{$i}}"></span>
                            @endfor
                        </div>

                        <div class="pull-right">
                            <div class="interview-price pull-right">
                                &#8377;{{$listing['price']}}
                            </div>
                        </div>

                        @if($listing['interviewer']->yearsOfExperience()!=0)
                            <div class="row interviewer-current-work">
                                <div>{{intval($listing['interviewer']->yearsOfExperience()/12) }}+ Years of Experience</div>
                            </div>
                        @endif
                        <div class="row interviewer-current-work">
                            <div>Interview for {{ $listing['subcategory']['name']  }}</div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="interviewer-summary">
                            {{$listing['description']}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="row">--}}
        {{--<div class="col-xs-5">--}}
            {{--<div class="btn-group pull-right">--}}
                {{--<button type="button" class="btn  btn-default dropdown-toggle sort-dropdown" data-toggle="dropdown"--}}
                        {{--aria-haspopup="true" aria-expanded="false">--}}
                    {{--<span class="selected-sort-value"> Select a date </span>&nbsp;&nbsp;<span class="caret"></span>--}}
                {{--</button>--}}
                {{--<ul class="dropdown-menu">--}}
                    {{--<li><a href="#" class="price_asc" data-value="price_asc">Price : Low to High</a></li>--}}

                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

<?php 
    var_dump($coupons)
?>
@endif
</div>
<div class="loading_modal"></div>
@include('footer')
@include('scripts')
</body>
</html>