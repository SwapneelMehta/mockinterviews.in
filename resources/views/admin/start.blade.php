<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Welcome to MockInterviews Admin Panel</h1>
        <div class="alert alert-info">
            <li>The database tables are listed on the left hand side</li>
            <li>Changes made via the Admin panel will be permanent once saved, so use carefully.</li>
        </div>
    </div>
</div>