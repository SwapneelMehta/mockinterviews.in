<!DOCTYPE HTML>
<html lang="en">
<head>
    @include('head')
    <link rel="stylesheet" href="{{ URL::asset('css/login.css') }}">
    <title>Forgot Password - MockInterview.in</title>
</head>
@include('navbar')
<body>
<div class="container-fluid">
    <div class="row mi_forgotPassword_row">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <span class="mi_authTitle_forgotPassword">Enter your email address to reset your password</span>
                </div>
            </div>
            @if (session('status'))
                <div class="alert alert-danger">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{!! action('AuthenticationController@postForgot') !!}">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="email_ig">
                            {{--<label for="email">Email</label>--}}
                            {{--<div class="input-group" id="email_ig">--}}
                            {{--<span class="input-group-addon"><i class="fa fa-envelope"></i></span>--}}
                            <input data-toggle="tooltip" data-placement="top" id="femail" type="text"
                                   class="form-control input-md" name="email" placeholder="">
                            {{--</div>--}}
                        </div>
                        <button id="forgot" class="disabled form-padding btn btn-md col-md-5 pull-right btn-yellow" value="register" name="submit" type="submit">Send Email
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('footer')
@include('scripts')
<script type="text/javascript" src="{{ URL::asset('js/authentication.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/authentication_forgot.js') }}"></script>
</body>
</html>