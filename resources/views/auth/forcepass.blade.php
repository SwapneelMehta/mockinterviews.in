<!DOCTYPE html>
<html>
<head>
    @include('head')
    <link rel="stylesheet" href="{{ URL::asset('css/login.css') }}">
    <title>Change Password - MockInterviews.in</title>
</head>
@include('navbar')
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <span class="mi_authTitle_forgotPassword">Change your password</span>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <span class="mi_authTitle">New password must be atleast 8 characters in length and must contain atleast one alphabet and atleast one digit.</span>
                </div>
            </div>
            <br/>
            @if (session('status'))
                <div class="alert alert-danger">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{!! action('AuthenticationController@postForcePassword') !!}">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="email_ig">
                            <label for="new">Old password given by administrator</label>
                        {{--<div class="input-group">--}}
                            {{--<span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>--}}
                            <input id="old" class="form-control input-md" type="password" name="current"
                                   placeholder="">
                        {{--</div>--}}
                        </div>
                        <div class="form-group" id="email_ig">
                            <label for="new">New password</label>
                        {{--<div class="input-group">--}}
                            {{--<span class="input-group-addon"><i class="fa fa-lock"></i></span>--}}
                            <input id="new" class="form-control input-md" type="password" name="new"
                                   placeholder="">
                        {{--</div>--}}
                        </div>

                        <div class="form-group" id="email_ig">
                            <label for="cnew">Confirm password</label>
                            {{--<div class="input-group">--}}
                            {{--<span class="input-group-addon"><i class="fa fa-lock"></i></span>--}}
                            <input id="cnew" class="form-control input-md" type="password" name="cnew"
                                   placeholder="">
                        {{--</div>--}}
                        </div>
                        <input id="userid" type="hidden" name="userid" value="{{ session('userid') }}">
                        <button id="submit" class="disabled form-padding btn btn-md col-md-5 pull-right btn-yellow"
                                value="change" name="submit" type="submit">Change Password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('footer')
@include('scripts')
<script type="text/javascript" src="{{ URL::asset('js/authentication.js') }}"></script>
<script type="text/javascript" src="{!! URL::asset('js/authentication_forcepass.js') !!}"></script>
</body>
</html>