<!DOCTYPE html>
<html>
<head>
    @include('head')
    <link rel="stylesheet" href="{{ URL::asset('css/login.css') }}">
    <title>Login - MockInterviews.in</title>
</head>
@include('navbar')
<body class="bg">
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <h4 class="mi_authTitle">Login or <a href="register/interviewee">Register</a></h4>
            <div class="row">
                <div class="col-xs-4 col-xs-offset-2">
                    <a href="{{ route('auth', ['login','linkedin']) }}" class="btn btn-md btn-social btn-linkedin btn-block">
                        <i class="fa fa-linkedin"></i>
                        <span class="">LinkedIn</span>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="{{ route('auth', ['login','google']) }}" class="btn btn-md btn-social btn-google btn-block">
                        <i class="fa fa-google-plus"></i>
                        <span class="">Google+</span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <div class="or-container">
                        <hr class="or-hr">
                        <div id="or">or</div>
                    </div>
                </div>
            </div>
            @if (session('status'))
                <div class="alert alert-danger">
                    {{ session('status') }}
                </div>
            @endif

            <form id="login_form" class="" action="{!! action('AuthenticationController@postLogin') !!}" autocomplete="off"
                  method="POST">
                {!! csrf_field() !!}

                <div class="row">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="email_ig">
                            <label for="email">Email</label>
                            <input data-toggle="tooltip" data-placement="top" id="email" type="text"
                                   class="form-control input-md" name="email" placeholder="">
                        </div>
                        <span class="help-block"></span>
                        <div class="form-group" id="pass_ig">
                            <label for="password">Password</label>
                            <input data-toggle="tooltip" data-placement="top" id="password" type="password"
                                   class="form-control input-md" name="password" placeholder="">
                        </div>
                        <input id="redirect" type="hidden">
                        <span class="help-block"></span>



                        <div class="row">
                            <div class="checkbox mi_checkbox pull-left">
                                <label><input name="remember_me" type="checkbox" value="">Remember Me</label>
                            </div>

                            <span class="mi_forgotPassword pull-right">
                                <a href="{{URL::action("AuthenticationController@getForgot")}}">Forgot password?</a>
                            </span>
                        </div>

                        <button class="btn btn-md col-md-5 pull-right btn-yellow" id="login" type="button">Login
                        </button>
                    </div>

                </div>


            </form>
        </div>
    </div>
</div>
<div class="loading_modal"></div>
@include('footer')
@include('scripts')
<script type="text/javascript" src="{{ URL::asset('js/authentication.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/authentication_login.js') }}"></script>
</body>
</html>