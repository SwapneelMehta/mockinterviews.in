<!DOCTYPE html>
<html>
<head>
    @include('head')
    <link rel="stylesheet" href="{{ URL::asset('css/login.css') }}">
    <title>Complete Profile - MockInterviews.in</title>
</head>
@include('navbar')
<body class="bg">
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            <h4 class="mi_authTitle">Welcome! Before you start using the site, we will need a few additional details from you</h4>

            @if (session('status'))
                <div class="alert alert-danger">
                    {{ session('status') }}
                </div>
            @endif

            <form id="save_profile_form" class="" action="{!! action('AuthenticationController@postMinimumCompleteProfile') !!}" autocomplete="off" method="POST">
                {!! csrf_field() !!}

                <div class="row">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="date_ig">
                            <label for="dob">Date of birth</label>
                            <input data-toggle="tooltip" data-placement="top" id="date" type="text"
                                   class="form-control input-md" name="dob" placeholder="">
                        </div>
                        <div class="form-group" id="phone_ig">
                            <label for="phone_number">Phone number</label>
                            <input data-toggle="tooltip" data-placement="top" id="phone" type="number"
                                   class="form-control input-md" name="phone_number" placeholder="">
                        </div>
                        <div class="form-group" id="city_ig">
                            <label for="city">City</label>
                            <select data-toggle="tooltip" data-placement="top" id="city"
                                   class="form-control input-md" name="city">
                                @include('auth.cities')
                            </select>
                        </div>
                        <div class="form-group" id="gender_ig">
                            <label for="gender">Gender</label>
                            <div>
                                <label class="radio-inline">
                                    <input  id="genderM" type="radio" name="gender" placeholder="" value="M">Male
                                </label>
                                <label class="radio-inline">
                                    <input  id="genderF" type="radio" name="gender" placeholder="" value="F">Female
                                </label>
                                <label class="radio-inline">
                                    <input  id="genderO" type="radio" name="gender" placeholder="" value="O" checked>Other/Do not wish to specify
                                </label>
                            </div>
                        </div>
                        <div class="form-group" id="bio_ig">
                            <label for="bio">Professional Bio/Summary(140 to 5000 characters)</label>
                            <textarea rows="7" data-toggle="tooltip" data-placement="top" id="bio" class="form-control" name="bio" placeholder=""></textarea>
                        </div>
                        <input id="redirect" type="hidden">
                        <button class="btn btn-md col-md-5 pull-right btn-yellow" type="button" id="save">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="loading_modal"></div>
@include('footer')
@include('scripts')
<script type="text/javascript" src="{{ URL::asset('js/authentication.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/authentication_complete_profile.js') }}"></script>
</body>
</html>