<!DOCTYPE html>
<html>
<head>
    @include('head')
    <link rel="stylesheet" href="{{ URL::asset('css/login.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/cropper.min.css') }}">
    <title>Register - MockInterviews.in</title>
</head>

@include('navbar')
<body class="bg">
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <h4 class="mi_authTitle">Register as Interviewee or  <a href="{{action('AuthenticationController@getRegisterInterviewer')}}">Register as Interviewer</a></h4>
            <div class="row">
                <div class="col-xs-4 col-xs-offset-2">
                    <a href="{{ route('auth', ['interviewee','linkedin']) }}" class="btn btn-md btn-social btn-linkedin btn-block">
                        <i class="fa fa-linkedin"></i>
                        <span class="">LinkedIn</span>
                    </a>
                </div>
                <div class="col-xs-4">
                    <a href="{{ route('auth', ['interviewee','google']) }}" class="btn btn-md btn-social btn-google btn-block">
                        <i class="fa fa-google-plus"></i>
                        <span class="">Google+</span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <div class="or-container">
                        <hr class="or-hr">
                        <div id="or">or</div>
                    </div>
                </div>
            </div>
            @if (session('status'))
                <div class="alert alert-danger">
                    {{ session('status') }}
                </div>
            @endif
            <form id ="register_form" action={!! action('AuthenticationController@postRegister') !!}
                    autocomplete="off" method="POST">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-xs-8 col-xs-offset-2">
                        <input type="hidden" value="interviewee" name="type">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="fname_ig">
                                    <label for="fname">First Name</label>
                                    <input data-toggle="tooltip" data-placement="top" id="fname" type="text"
                                           class="form-control input-md " name="fname" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="lname_ig">
                                    <label for="lname">Last Name</label>
                                    <input data-toggle="tooltip" data-placement="top" id="lname" type="text"
                                           class="form-control input-md" name="lname" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="email_ig">
                            <label for="email">Email</label>
                            <input data-toggle="tooltip" data-placement="top" id="remail" type="text"
                                   class="form-control input-md" name="email" placeholder="">
                        </div>

                        <div class="form-group" id="pass_ig">
                            <label for="password">Password</label>
                            <input data-toggle="tooltip" data-placement="top" id="rpassword" type="password"
                                   class="form-control input-md" name="password" placeholder="">
                        </div>

                        <div class="form-group" id="cpass_ig">
                            <label for="cpassword">Confirm password</label>
                            <input data-toggle="tooltip" data-placement="top" id="crpassword" type="password"
                                   class="form-control input-md" name="cpassword" placeholder="">
                        </div>
                        <button class="form-padding btn btn-md btn-yellow col-md-5 pull-right" id="register"
                                value="register" name="register_button" type="button">Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="loading_modal"></div>
@include('footer')
@include('scripts')
<script type="text/javascript" src="{{ URL::asset('js/authentication.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/cropper.min.js') }}"></script>
<script type="text/javascript" src="{!! URL::asset('js/authentication_register.js') !!}"></script>
</body>
</html>