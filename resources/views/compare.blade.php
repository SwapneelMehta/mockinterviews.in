<!DOCTYPE html>
<html>
<head>
    <title>Compare Interviewers</title>
    @include('head')
    <link rel="stylesheet" type="text/css" href="{!!URL::asset('css/chosen.min.css')!!}">
    <link rel="stylesheet" type="text/css" href="{!!URL::asset('css/compare-interviewer.css')!!}">
</head>
@include('navbar')
<body>
<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

            <div class="panel panel-primary">
                <table class="table-bordered">
                    {{--Photo Basic Info--}}
                    <tr>
                        <th style="background-color: #ffffff;"></th>

                        @for($i = 0; $i < $compareCase; $i++)
                            <td>
                                <span id="close_button{{$i}}" class="fa fa-times pull-right"></span><br>
                                <div class="interviewer-basic-info" align="center">
                                    <a class="compare-a" href="/interviewer/profile/{{$id[$i]}}"><img width="90px"
                                                                                                      height="90px"
                                                                                                      src="{{ URL::asset("files/profile_images/".$profile_image_url[$i]) }} "></a><br>
                                    <div class="interviewer-name">
                                        <a class="compare-a" href="/interviewer/profile/{{$id[$i]}}">{{$name[$i]}}</a>
                                    </div>
                                    <div class="compare-star-rating">
                                        <?php $r = 0 ?>
                                        @for($k=1;$k<=intval($rating[$i]);$k++)
                                            <span class="fa fa-star" data-rating="{{$k}}"></span>
                                        @endfor
                                        @if(($rating[$i] - intval($rating[$i]))>=0.25 && ($rating[$i] - intval($rating[$i]))<=0.75)
                                            <span class="fa fa-star-half-o" data-rating="2"></span>
                                            <?php $r = 1 ?>
                                        @endif
                                        @for($k=intval($rating[$i])+$r;$k<5;$k++)
                                            <span class="fa fa-star-o" data-rating="{{$k}}"></span>
                                        @endfor
                                    </div>

                                </div>
                            </td>
                        @endfor
                        @for($j = $compareCase; $j < 2; $j++)
                            <td align="center">
                                <select class="compare-dropdown" id="drop{{$j}}">
                                    <span class="caret"></span>
                                    <option value="">Select an Interviewer</option>
                                    <optgroup label="Shortlisted">
                                        {{--@foreach({{/Session::get()}} as $user)--}}
                                        {{--<option value="{{$user['id']}}">{{$user['name']}}</option>--}}
                                        {{--@endforeach--}}
                                        {{--</optgroup>--}}
                                    </optgroup>
                                    <optgroup label="All Interviewers">
                                        @foreach($list as $user)
                                            <option value="{{$user['id']}}">{{$user['name']}}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </td>
                        @endfor
                    </tr>


                    {{--Summary--}}
                    <tr>
                        <div class="row-heading">
                            <th>Summary</th>
                        </div>

                        @for($i = 0; $i < $compareCase; $i++)
                            <td>
                                <div class="compare-bio-text">
                                    {{$bio[$i]}}
                                </div>
                            </td>
                        @endfor
                        @for($i = 0; $i < 2-$compareCase; $i++)
                            <td>
                                <div class="compare-nothing"></div>
                            </td>
                        @endfor
                    </tr>

                    {{--Education--}}
                    <tr>
                        <div class="row-heading">
                            <th>Education</th>
                        </div>
                        @for($j = 0; $j < $compareCase; $j++)
                            <td>

                                @foreach($education[$j] as $i)
                                    <div class="section-title"><b>{{$i['institution']}}</b>
                                        ({{$i['degree']}})
                                    </div>
                                    <div class="row interviewer-current-work">
                                        {{$i->start_date->formatLocalized('%B %Y')}}
                                        - {{$i->end_date->formatLocalized('%B %Y')}}
                                    </div>
                                @endforeach

                            </td>
                        @endfor
                        @for($i = 0; $i < 2-$compareCase; $i++)
                            <td>
                                <div class="compare-nothing"></div>
                            </td>
                        @endfor
                    </tr>

                    {{--Experience--}}
                    <tr>
                        <div class="row-heading">
                            <th>Experience</th>
                        </div>
                        @for($j = 0; $j < $compareCase; $j++)
                            <td>

                                @if(intval($yearsOfExp[$j]/12) >= 1)
                                    <div class="section-title">
                                        <div>{{ intval($yearsOfExp[$j]/12) }}+ Years of Experience</div>
                                    </div>
                                @endif
                                @foreach($experience[$j] as $i)

                                    <div class="section-title"><b>{{$i['title']}} in {{$i['company_name']}}</b><br>
                                        <div class="compare-experience-period">
                                            {{$i['start_date']->formatLocalized('%B %Y')}}
                                            - {{$i['end_date']->formatLocalized('%B %Y')}}
                                        </div>
                                    </div>

                                @endforeach

                            </td>
                        @endfor
                        @for($i = 0; $i < 2-$compareCase; $i++)
                            <td>
                                <div class="compare-nothing"></div>
                            </td>
                        @endfor
                    </tr>

                    {{--Skill--}}
                    <tr>
                        <div class="row-heading">
                            <th>Skills</th>
                        </div>
                        @for($j = 0; $j < $compareCase; $j++)
                            <td>
                                <div class="section-body">
                                    @foreach($skills[$j] as $i)
                                        <div class="section-title">
                                            <a class="compare-a"
                                               href="/listings?subcategories={{$i['name']}}#subcategories={{$i['name']}}">
                                                <b>{{$i['name']}}</b>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </td>
                        @endfor
                        @for($i = 0; $i < 2-$compareCase; $i++)
                            <td>
                                <div class="compare-nothing"></div>
                            </td>
                        @endfor
                    </tr>

                    {{--Review--}}
                    <tr>
                        <div class="row-heading">
                            <th>Reviews</th>
                        </div>
                        @for($j = 0; $j < $compareCase; $j++)
                            <td>
                                <div class="compare-review">

                                    @foreach($reviewsFor[$j] as $i)

                                        <div class="compare-review-by section-title">
                                            <a class="compare-a"
                                               href="/interviewer/profile/{{$reviewInt[$i['id']]}}">{{(\App\User::find($i['by_user_id'])['first_name'])}}
                                                {{(\App\User::find($i['by_user_id'])['last_name'])}}
                                            </a>
                                        </div>
                                        <div class="compare-review-rating">
                                            {{--@for($k = 1; $k <= 5; $k++)--}}
                                            {{--@if($k <= $i['rating'])--}}
                                            {{--<span class="fa fa-star" data-rating="{{$k}}"></span>--}}
                                            {{--@else--}}
                                            {{--<span class="fa fa-star-o" data-rating="{{$k}}"></span>--}}
                                            {{--@endif--}}
                                            {{--@endfor--}}
                                            <?php $r = 0 ?>
                                            @for($k=1;$k<=intval($i['rating']);$k++)
                                                <span class="fa fa-star" data-rating="{{$k}}"></span>
                                            @endfor
                                            @if(($i['rating'] - intval($i['rating']))>=0.25 && ($i['rating'] - intval($i['rating']))<=0.75)
                                                <span class="fa fa-star-half-o" data-rating="2"></span>
                                                <?php $r = 1 ?>
                                            @endif
                                            @for($k=intval($i['rating'])+$r;$k<5;$k++)
                                                <span class="fa fa-star-o" data-rating="{{$k}}"></span>
                                            @endfor
                                        </div>

                                    @endforeach

                                </div>
                            </td>
                        @endfor
                        @for($i = 0; $i < 2-$compareCase; $i++)
                            <td>
                                <div class="compare-nothing"></div>
                            </td>
                        @endfor
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
@include('footer')
@include('scripts')
<script type="text/javascript" src="{!!URL::asset('js/chosen.jquery.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/chosen.proto.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/compare-interviewer.js')!!}"></script>
</body>

</html>
