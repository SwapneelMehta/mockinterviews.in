<!DOCTYPE html>
<html>
<head>
    @include('head')
    <link rel="stylesheet" type="text/css" href="{!!URL::asset('css/cart.css')!!}">
</head>
@include('navbar')
<body>
<div class="container">
@if(!$listings->isEmpty())
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        @foreach($listings as $listing)
            <div class="alert alert-success" id="{{ 'listing-'.$listing['id'] }}">
                <div class="row interview-row">
                    <div class="interviewer-img col-xs-2">
                        <img src="{{URL::asset('files/profile_images/')}}/{{$listing['interviewer']['user']['profile_image_url']}}">
                    </div>
                    <div class="interview-details col-xs-10">
                        <div class="row">
                            <div class="interviewer-name"><a class="black-link" href="interviewer/profile/{{$listing['interviewer_id']}}">{{$listing['interviewer']['user']['first_name']}} {{$listing['interviewer']['user']['last_name']}}</a> @if($listing['interviewer']['verified'] == 1)<i class="fa fa-check-circle" data-toggle="tooltip" data-placement="bottom" title="Certified Interviewer"></i> @endif </div>
                            <div class="star-rating">
                                <?php $r=0 ?>
                                @for($i=1;$i<=intval($listing['interviewer']['rating']);$i++)
                                    <span class="fa fa-star" data-rating="{{$i}}"></span>
                                @endfor
                                @if(($listing['interviewer']['rating'] - intval($listing['interviewer']['rating']))>=0.25 && ($listing['interviewer']['rating'] - intval($listing['interviewer']['rating']))<=0.75)
                                    <span class="fa fa-star-half-o" data-rating="2"></span>
                                    <?php $r=1 ?>
                                @endif
                                @for($i=intval($listing['interviewer']['rating'])+$r;$i<5;$i++)
                                    <span class="fa fa-star-o" data-rating="{{$i}}"></span>
                                @endfor
                            </div>

                            <div class="pull-right">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <form action="/checkout" method="POST">
                                {{ csrf_field() }}
                                    <input type="hidden" name="listing_id" value="{{ $listing->id }}"/>
                                    <input type="submit" class="btn-default shortlist-btn btn pull-right proceed-btn" id="{{ 'checkout-'.$listing->id }}" value="Checkout"/>
                                </form>
                                <div class="interview-price pull-right">&#8377;{{$listing['price']}}</div>
                            </div>

                            @if($listing['interviewer']->yearsOfExperience()!=0)
                                <div class="row interviewer-current-work">
                                    <div>{{intval($listing['interviewer']->yearsOfExperience()/12) }}+ Years of Experience</div>
                                </div>
                            @endif
                            <div class="row interviewer-current-work">
                                <div>Interview for {{ $listing['subcategory']['name']  }}</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="interviewer-summary">
                                {{$listing['description']}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
@else
    <center><i><h4>Your Shortlist is empty!</h4></i></center>
    <div class="row" id="">
        <a class="col-xs-4 col-xs-offset-4 btn btn-default yellow-btn" id="" href="/listings">Back to Listings</a>
    </div>
@endif
</div>
<br>
@include('footer')
@include('scripts')
<script type="text/javascript" src="{!! URL::asset('js/jquery.session.min.js') !!}"></script>
<script type="text/javascript" src="{!! URL::asset('js/cart.js') !!}"></script>
<div class="loading_modal"></div>
</body>
</html>