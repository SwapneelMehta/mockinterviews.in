<!DOCTYPE html>
<html>
<head>
    @include('head')
    <link rel="stylesheet" type="text/css" href="{!!URL::asset("select2.min.css")!!}"/>
    <link rel="stylesheet" type="text/css" href="{!!URL::asset("css/cropper.min.css")!!}">
    <link rel="stylesheet" type="text/css" href="{!!URL::asset("css/star-rating.min.css")!!}" media="all" />
    <link rel="stylesheet" type="text/css" href="{!!URL::asset("css/interviewer_edit.css")!!}">
</head>

@include('navbar')


<body class="bg">
    <div class="container-fluid">

        <div class="container" style="padding-top: 0px;">

            <div class="col-xs-10 col-xs-offset-1">
                <h1 class="page-header">Edit Profile</h1>

                <div class="row">
                    <!-- left column -->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="text-center">
                            <div class="img-preview preview-lg" id="preview" style="width: 200px;height: 200px; overflow: hidden;margin:0 auto">
                                <img id = "avatarimg" src="{{$interviewer->user->profile_image_url->link()}}" class="avatar img-thumbnail" alt="avatar">
                            </div>

                            <h6>Upload a different photo...</h6>
                            <input id="inputImage" accept="image/*" type="file" class="text-center center-block well well-sm">
                            <br>
                            <br>
                            <h6>Update your resume...</h6>
                            <input id="inputFile" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" type="file" class="text-center center-block well well-sm">
                            {{--<input type="inputFile" type = "file" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="text-center center-block well well-sm">--}}
                        </div>
                    </div>
                    <!-- edit form column -->
                    <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
                        <div class="alert alert-info alert-dismissable">
                            <a class="panel-close close" data-dismiss="alert">×</a>
                            <i class="fa fa-coffee"></i>
                            Please Save the updated changes.
                        </div>
                        <h3>Personal info</h3>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">First name:</label>
                                <div class="col-lg-8">
                                    <input class="form-control" id="pd_firstname" value="{{$interviewer->user->first_name}}" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Middle name:</label>
                                <div class="col-lg-8">
                                    <input class="form-control" id="pd_middlename" value="{{$interviewer->user->middle_name}}" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Last name:</label>
                                <div class="col-lg-8">
                                    <input class="form-control" id="pd_lastname" value="{{$interviewer->user->last_name}}" type="text">
                                </div>
                            </div>

                            {{--email was here--}}

                            <div class="form-group">
                                <label class="col-md-3 control-label">Phone:</label>
                                <div class="col-md-8">
                                    <input class="form-control" id="pd_phoneno" value="{{$interviewer->user->phone_no}}" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Old password:</label>
                                <div class="col-md-8">
                                    <input class="form-control" id="pd_oldpassword" value="" type="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">New password:</label>
                                <div class="col-md-8">
                                    <input class="form-control" id="pd_newpassword" value="" type="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Confirm password:</label>
                                <div class="col-md-8">
                                    <input class="form-control" id="pd_confirmnewpassword" value="" type="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">City:</label>
                                <div class="col-md-8">
                                    <input class="form-control" id="pd_city" value="{{$interviewer->user->city}}" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Bio:</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" id="pd_bio" rows="5" id="bio">{{$interviewer->user->bio}}</textarea>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                <div class="row section-panel">
                    <div class="section-title">
                        Skills
                    </div>
                    <div class="container pull-left" style="padding-bottom: 5px; margin-top: 15px">
                        <div class="row">
                            <div class="col-xs-3" style="padding-left: 0px!important;">
                                <form class="form-search">
                                    <div class="input-group">
                                        {{--<input type="text" class="form-control" id = "newSkillTxt" placeholder="Add Skill">--}}
                                        <select class="js-example-tokenizer col-xs-12" multiple="" tabindex="-1" aria-hidden="true" id="skillselect">
                                            @foreach($skillset as $skill)
                                                <option value="{{$skill->name}}">{{$skill->name}}</option>
                                            @endforeach
                                        </select>
                                                <span class="input-group-btn addSkillBtn2">
                                                    {{--<button class="btn btn-search" type="button" id="addSkillBtn">Add <i class="fa fa-plus"></i></button>--}}
                                                    <button class="btn btn-default" type="button">
                                                        <span class="caret"></span>
                                                    </button>
                                                </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="section-body" id = "skillContainer">
                        @foreach($interviewer->user->skills as $skill)
                            <div class = "skill-tag" style = "display: inline-block; margin: 5px;">
                                <a href="/listings?subcategories={{$skill->name}}#subcategories={{$skill->name}}">{{$skill->name}}</a>
                                <a style="padding-left: 5px;cursor: pointer;" class = "tagRemove" user="{{$interviewer->user_id}}" tag="{{$skill->name}}" other = "0">  x</a>
                            </div>
                        @endforeach
                        @foreach($interviewer->user->extraSkills as $skill)
                            <div class = "skill-tag" style = "display: inline-block; margin: 5px;">
                                <a href="/listings?q={{$skill->name}}#q={{$skill->name}}">{{$skill->name}}</a>
                                <a style="padding-left: 5px;cursor: pointer;" class = "tagRemove" user="{{$interviewer->user_id}}" tag="{{$skill->name}}" other = "1">  x</a>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-5">
                        <div class="row section-panel">
                            <div class="section-title">
                                Work Experience
                                <i class="pull-right fa fa-plus" id="addExp" style="padding-top: 5px!important;cursor: pointer;"></i>
                            </div>
                            <div class="section-body" id = "workExContainer">
                                @foreach($interviewer->user->experience->sortByDesc('start_date') as $work)
                                    <div class="row timeline-details bottomSep">
                                        <div class = "row">
                                            <div class = "col-xs-11">
                                                <input style="width: 250px;" class="form-control" type = "text" id="companyName" placeholder="Company Name" value="{{$work->company_name}}">
                                            </div>
                                            <div class = "col-xs-1">
                                                <a class="pull-right btn shortlist-btn delExp"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                        <div class="row interviewer-current-work" style="padding-top: 5px!important;">
                                            <div><input style="width: 200px;" class="form-control input-sm" type = "text" id="workTitle" placeholder="Work Title" value="{{$work->title}}"></div>
                                        </div>
                                        <div class="row interviewer-current-work" style="padding-top: 5px!important;">
                                            <div><input style="width: 200px;" class="form-control input-sm" type = "text" id="location" placeholder="Location" value="{{$work->location}}"></div>
                                        </div>
                                        <div class="row interviewer-current-work col-xs-10 pull-left" style="padding-left: 0px!important;padding-top: 5px;">
                                            <div class="input-daterange input-group sandbox-container">
                                                <input placeholder="start date" type="text" class="input-sm form-control" name="start" value="{{$work->start_date->formatLocalized('%B %Y')}}"/>
                                                <span class="input-group-addon">to</span>
                                                <input placeholder="end date" type="text" class="input-sm form-control" name="end" value="{{$work->end_date->formatLocalized('%B %Y')}}"/>
                                            </div>
                                        </div>
                                        <div class="row interviewer-current-work col-xs-10 pull-left" style="padding-left: 0px!important;padding-top: 5px;">
                                            <textarea class="form-control" rows="3" id="bio" placeholder="Bio">{{$interviewer->user->bio}}</textarea>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-offset-1 col-xs-5">
                        <div class="row section-panel">
                            <div class="section-title">
                                Education
                                <i class="pull-right fa fa-plus" id="addEdu" style="padding-top: 5px!important;cursor: pointer;"></i>
                            </div>
                            <div class="section-body" id = "eduContainer">
                                @foreach($interviewer->user->education->sortByDesc('start_date') as $education)
                                    <div class="row timeline-details bottomSep">
                                        <div class = "row">
                                            <div class = "col-xs-11">
                                                <input style="width: 250px;" class="form-control" type = "text" id="institution" placeholder="Institution" value="{{$education->institution}}">
                                            </div>
                                            <div class = "col-xs-1">
                                                <a class="pull-right btn shortlist-btn delEdu"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                        <div class="row interviewer-current-work" style="padding-top: 5px!important;">
                                            <div><input style="width: 200px;" class="form-control input-sm" type = "text" id="degree" placeholder="Degree" value="{{$education->degree}}"></div>
                                        </div>
                                        <div class="row interviewer-current-work col-xs-10 pull-left" style="padding-left: 0px!important;padding-top: 5px;">
                                            <div class="input-daterange input-group sandbox-container">
                                                <input placeholder="start date" type="text" class="input-sm form-control" name="start" value="{{$education->start_date->formatLocalized('%B %Y')}}"/>
                                                <span class="input-group-addon">to</span>
                                                <input placeholder="end date" type="text" class="input-sm form-control" name="end" value="{{$education->end_date->formatLocalized('%B %Y')}}"/>
                                            </div>
                                        </div>

                                    </div>

                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row section-panel bottomSep">
                    <div class="section-title">
                        Listings
                        <i class="pull-right fa fa-plus" id="addLis" style="padding-top: 5px!important;cursor: pointer;"></i>
                    </div>
                    <div class="section-body" id = "listingContainer">
                        {{--Iterate interviewers listings here--}}
                        @foreach($interviewer->listings as $listing)
                            @if($listing->disabled==0)
                                <div class="row timeline-details bottomSep">
                                    <div class = "row">
                                        <div class = "col-xs-11">
                                            <div class="col-xs-12" style="padding-left: 0px!important;">
                                                <form class="form-search">
                                                    <div class="input-group col-xs-4">
                                                        {{--<input type="text" class="form-control" id = "newSkillTxt" placeholder="Add Skill">--}}
                                                        <select class="js-example-responsive" style="width: 100%">
                                                            @foreach($subcats as $cat)
                                                                @if($cat->name==$listing->subcategory->name)
                                                                    <option value = "{{$cat->id}}"selected="selected">{{$cat->name}}</option>
                                                                @else
                                                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </form>
                                            </div>
                                            {{--<input style="width: 250px;" class="form-control" type = "text" id="listingCat" placeholder="Subcategory Name" value="{{$listing->subcategory->name}}">--}}
                                        </div>
                                        <div class = "col-xs-1">
                                            <a class="pull-right btn shortlist-btn delLis">Disable</a>
                                        </div>
                                    </div>
                                    <div class="row interviewer-current-work" style="padding-top: 5px!important;">
                                        <div><input style="width: 200px;" class="form-control input-sm" type = "text" id="listingPrice" placeholder="Price" value="{{$listing->price}}"></div>
                                    </div>
                                    <div class="row interviewer-current-work col-xs-10 pull-left" style="padding-left: 0px!important;padding-top: 5px;">
                                        <textarea class="form-control" rows="3" id="bio" placeholder="Description">{{$listing->description}}</textarea>
                                    </div>
                                </div>
                            @else
                                <div class="row timeline-details disabled bottomSep">
                                    <div class = "row">
                                        <div class = "col-xs-11">
                                            <input style="width: 250px;" class="form-control" type = "text" id="listingCat" placeholder="Subcategory Name" value="{{$listing->subcategory->name}}" disabled>
                                        </div>
                                        <div class = "col-xs-1">
                                            <a class="pull-right btn shortlist-btn delLis">Enable</a>
                                        </div>
                                    </div>
                                    <div class="row interviewer-current-work" style="padding-top: 5px!important;">
                                        <div><input style="width: 200px;" class="form-control input-sm" type = "text" id="listingPrice" placeholder="Price" value="{{$listing->price}}" disabled></div>
                                    </div>
                                    <div class="row interviewer-current-work col-xs-10 pull-left" style="padding-left: 0px!important;padding-top: 5px;">
                                        <textarea class="form-control" rows="3" id="bio" placeholder="Description" disabled>{{$listing->description}}</textarea>
                                    </div>
                                </div>
                            @endif

                        @endforeach
                        {{--Dropdown list search for subcats for skills and listing is left, calm thy tits.--}}
                    </div>
                </div>
            </div>

            <div id = "savebtns" class="form-group pull-right" style="padding-right: 100px">
                <button type="button" id = "canedit" class="btn btn-default" >Close</button>
                <button type="button" id="saveedit" class="shortlist-btn btn btn-primary">Save Changes</button>
            </div>

        </div>
    </div>

    {{--parens modal--}}

    <div class="modal fade" id="modal_crop" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Crop Profile Image</h4>
                </div>
                <div class="modal-body">
                    <div id="cropper_image">
                        <img id="imageToCrop" style="width: 100%;">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="loading_modal"></div>
    @include('footer')
    @include('scripts')
    <script type="text/javascript" src="{!!URL::asset('js/jquery.serialScroll.min.js')!!}"></script>
    <script type="text/javascript" src="{!!URL::asset('js/star-rating.min.js')!!}"></script>
    <script type="text/javascript" src="{!!URL::asset('js/cropper.min.js')!!}"></script>
    <script type="text/javascript" src="{!!URL::asset('js/select2.min.js')!!}"></script>
    <script type="text/javascript" src="{!!URL::asset('js/interviewer_edit.js')!!}"></script>
</body>


</html>