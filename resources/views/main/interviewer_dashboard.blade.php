<!DOCTYPE html>
<html>
<head>
    @include('head')
    <link rel="stylesheet" type="text/css" href="{!!URL::asset('css/jquery-ui.min.css')!!}">
    <link rel="stylesheet" type="text/css" href="{!!URL::asset('css/interviewer_dashboard.css')!!}">
</head>

@include('navbar')


<body class="bg">
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-7 col-xs-offset-1 ">
            <div class="interviewer-img col-xs-3">
                <img src="{{$interviewer->user->profile_image_url->link()}}">
            </div>
            <div class="interview-details col-xs-9">
                <div class="interviewer-name">{{$interviewer->user->first_name.' '.$interviewer->user->last_name}} @if($interviewer->verified == 1)
                        <i class="fa fa-check-circle" data-toggle="tooltip" data-placement="bottom"
                           title="Certified Interviewer"></i> @endif </div>
                <div class="star-rating">
                    <?php $r = 0 ?>
                    @for($i=1;$i<=intval($interviewer->avgRating());$i++)
                        <span class="fa fa-star" data-rating="{{$i}}"></span>
                    @endfor
                    @if(($interviewer->avgRating() - intval($interviewer->avgRating()))>=0.25 && ($interviewer->avgRating() - intval($interviewer->avgRating()))<=0.75)
                        <span class="fa fa-star-half-o" data-rating="2"></span>
                        <?php $r = 1 ?>
                    @endif
                    @for($i=intval($interviewer->avgRating())+$r;$i<5;$i++)
                        <span class="fa fa-star-o" data-rating="{{$i}}"></span>
                    @endfor
                </div>
                @if(!$interviewer->user->experience->isEmpty())
                    <div class="row interviewer-current-work">
                        <div>{{ intval($interviewer->yearsOfExperience()/12) }}+ Years of Experience</div>
                    </div>
                @endif
                <div class="row interviewer-current-work">
                    @if(!$interviewer->user->experience->isEmpty())
                        <div>{{ $interviewer->user->experience->sortByDesc('start_date')->first()->title.' ,'.$interviewer->user->experience->sortByDesc('start_date')->first()->company_name }}</div>
                    @endif
                </div>
                @if(Auth::check() && $interviewer->user->id == Auth::user()->id)
                    <div class="row socio-links">
                        <span class="fa fa-linkedin-square fa-2x fa-socio-links-padding" data-placement="bottom" data-toggle="tooltip" title="LinkedIn Profile"></span>
                        <span class="fa fa-google-plus-square fa-2x fa-socio-links-padding"  data-placement="bottom" data-toggle="tooltip" title="Google+ Profile"></span>
                        <span class="fa fa-file-text fa-2x fa-socio-links-padding" data-placement="bottom" data-toggle="tooltip" title="Resume"></span>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-9 col-xs-offset-1 ">
            <div class="tabbable-panel">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_profile" data-toggle="tab">
                                Profile</a>
                        </li>
                        @if(Auth::check() && $interviewer->user->id == Auth::user()->id)
                            <li>
                                <a href="#tab_availability" data-toggle="tab">
                                    Availability</a>
                            </li>
                            <li>
                                <a href="#tab_appointments" data-toggle="tab">
                                    Appointments</a>
                            </li>
                        @endif
                        <li>
                            <a href="#tab_listings" data-toggle="tab">
                                Listings</a>
                        </li>
                        @if(Auth::check() && $interviewer->user->id == Auth::user()->id)
                            <li>
                                <a href="#tab_history" data-toggle="tab">
                                    History</a>
                            </li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        @include('main.interviewer_tabs.profile')
                        @if(Auth::check() && $interviewer->user->id == Auth::user()->id)
                            @include('main.interviewer_tabs.availability')
                            @include('main.interviewer_tabs.appointments')
                            @include('main.interviewer_tabs.history')
                        @endif
                        @include('main.interviewer_tabs.listings')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="hide">
    <hr>

    {{--<label>Review:</label>--}}

    <div class="form-group">
        <textarea class="form-control" id="review" placeholder="Review Text"></textarea>
    </div>
    <div class="row">
        <div class="form-group pull-left">
            <input id="rating-input" class="rating" min="0" max="5" step="0.5" data-size="sm" type="number"
                   data-symbol="&#xf005;" data-glyphicon="false" data-rating-class="rating-fa" value="1">
        </div>
        <div id="revbtns" class="form-group pull-right">
            <button type="button" id="cancelbtn" class="btn btn-default">Close</button>
            <button type="button" id="submitbtn" class="add-review-btn btn btn-primary">Save</button>
        </div>
    </div>
</div>
<div class="loading_modal"></div>
@include('footer')
@include('scripts')
<script type="text/javascript" src="{!!URL::asset('js/interviewer_dashboard.js')!!}"></script>
</body>


</html>