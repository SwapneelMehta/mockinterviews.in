<!DOCTYPE html>
<html>
<head>
    @include('head')
</head>
@include('navbar')
<body class="bg">
<div class="container-fluid">
    <div class="filter-panel">
        <div class="panel">
            <div class="panel-heading">
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                <h2 class="panel-title">Years of Experience</h2>

            </div>
            <div class="panel-body">
                <div id="yearsofexperience_help"></div>
                <input id="yearsofexperience" type="text"/><br/>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                <h3 class="panel-title subcategorytitle">Sub Category </h3><a
                        class=" subcategories-clear link">Clear</a>
            </div>
            <div class="panel-body">
                <div class="scroll-div">
                    @foreach($subcategories->sortBy('name') as $subcategory)
                        <div class="checkbox">
                            <label><input class="subcategories-option"
                                          value="{{$subcategory->name}}" type="checkbox"/>{{$subcategory->name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                <h3 class="panel-title companiestitle">Companies </h3><a class=" companies-clear link">Clear</a>
            </div>
            <div class="panel-body">
                <div class="scroll-div">
                    @foreach($companies->sortBy('company_name') as $company)
                        <div class="checkbox">
                            <label><input class="companies-option"
                                          value="{{$company->company_name}}" type="checkbox"/>{{$company->company_name}}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                <h3 class="panel-title">Interviewer Rating</h3>
            </div>
            <div class="panel-body">
                <div id="rating_help"></div>
                <input id="rating" type="text"/><br/>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                <h3 class="panel-title">Date And Time</h3>
            </div>
            <div class="panel-body date-panel-body">
                <div class="date-field-group col-xs-12">
                    <div>From Date :</div>
                    <input type='text' class="form-control" id='datetimepickerstart'/>
                </div>
                <div class="date-field-group col-xs-12">
                    <div>To Date :</div>
                    <input type='text' class="form-control" id='datetimepickerend'/>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                <h3 class="panel-title">Price Range</h3>
            </div>
            <div class="panel-body">
                <div id="price_help"></div>
                <input id="price" type="text" class="col-xs-1" value="" data-slider-min="0"
                       data-slider-max="@if($maxprice) {{$maxprice}} @else 0 @endif" data-slider-step="1"
                       data-slider-value="[0,@if($maxprice) {{$maxprice}} @else 0 @endif]"/>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                <h3 class="panel-title">Certified by MI</h3>
            </div>
            <div class="panel-body">
                <div class="checkbox">
                    <label><input value="true" class="certified-option" type="checkbox"/>Yes</label>
                </div>
            </div>
        </div>
    </div>
    <div class="main listings-panel">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12" id="listingsdiv">
                    <div class="row">
                        <div class="col-xs-7 invisible query-results-div"><!--
                         -->Showing search results for <span class="query-text"></span>&nbsp;<a class=" search-clear link">Clear Search</a>
                        </div>
                        <div class="col-xs-5">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn  btn-default dropdown-toggle sort-dropdown" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    <span class="selected-sort-value"></span>&nbsp;&nbsp;<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" class="price_asc" data-value="price_asc">Price : Low to High</a></li>
                                    <li><a href="#" class="price_desc" data-value="price_desc">Price : High to Low</a></li>
                                    <li><a href="#" class="rating_desc" data-value="rating_desc">Rating : High to Low</a></li>
                                    <li><a href="#" class="rating_asc" data-value="rating_asc">Rating : Low to High</a></li>
                                    <li><a href="#" class="yearsofexp_asc" data-value="yearsofexp_asc">Work Experience : Low to
                                            High</a></li>
                                    <li><a href="#" class="yearsofexp_desc" data-value="yearsofexp_desc">Work Experience : High to
                                            Low</a></li>
                                    <li><a href="#" class="date_asc" data-value="date_asc">Availability : Earliest First</a></li>
                                    <li><a href="#" class="date_desc" data-value="date_desc">Availability : Latest First</a></li>
                                </ul>
                            </div>
                            <div class="pull-right" style="margin-top: 6px;margin-right: 9px;">Sort By:</div>
                        </div>
                    </div>
                    <div class="row" id="loadmore-row">
                        <button class="col-xs-4 col-xs-offset-4 btn btn-default yellow-btn" id="loadmore">Load More</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="loading_modal"></div>
@include('footer')
@include('scripts')
<script type="text/javascript" src="{!!URL::asset('js/listings.js')!!}"></script>
</body>
</html>