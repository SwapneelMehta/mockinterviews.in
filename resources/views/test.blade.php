<!DOCTYPE html>
<html>
<head>
    @include('head')
    <link rel="stylesheet" type="text/css" href="{!!URL::asset('css/interviewer_dashboard.css')!!}">
    <link rel="stylesheet" type="text/css" href="{!!URL::asset('css/jquery-ui.css')!!}">
    <link rel="stylesheet" type="text/css" href="{!!URL::asset('css/star-rating.min.css')!!}">
</head>

@include('navbar')
<body class="bg">
<div class="container-fluid">
    {{var_dump($availabilites_as_unix_timestamp)}}
    <br/>
    <br/>
    <br/>
    {{var_dump($appointments_as_unix_timestamp)}}
    <div class="tab-pane" id="tab_availability">
        <div class="availability-widget">
            <div class="panel panel-default panel-left-absolute">
                <div class="panel-heading">
                    <div class="panel-title">Good Heavens<br/>look at teh time</div>
                </div>
                <div class="panel-body">
                    <?php
                    $today = Carbon\Carbon::today();
                    $start = $today->copy();
                    $end = $today->copy();
                    $start->addHours(9);
                    $end->addHours(20.5);
                    ?>
                    @while($start<=$end)
                        <div class="left-absolute-column-cell">{{$start->format('h:i A')}}
                            to {{$start->addHour()->format('h:i A')}}</div>
                    @endwhile
                </div>
            </div>
            <div id="slideshow">
                <div>
                    <?php $today = Carbon\Carbon::today() ?>
                    @for($i=0;$i<30;$i++)
                        <div class="cal-item panel panel-default @if($i==0) adjust-margin-for-absolute @endif">
                            <div class="panel-heading">
                                <div class="panel-title rotate">{{ $today->format('jS F')}}</div>
                            </div>
                            <div class="panel-no-padding panel-body">
                                <?php
                                $start = $today->copy();
                                $end = $today->copy();
                                $start->addHours(9);
                                $end->addHours(20.5);
                                ?>
                                @while($start<=$end)
                                    <div value="{{ $start->timestamp }}-{{$start->copy()->timestamp}}"
                                         class="timeslot @if($start<Carbon\Carbon::now()) disabled
                                                                                          @elseif(isset($appointments_as_unix_timestamp[$start->timestamp])) bg-yellow disabled
                                                                                          @elseif(isset($availabilites_as_unix_timestamp[$start->timestamp])) bg-green
                                                                                          @else bg-red @endif time-padding list-group-item"></div>
                                    <?php $start->addHour() ?>
                                @endwhile
                            </div>
                        </div>
                        <?php $today->addDay() ?>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer')
@include('scripts')
<script src="{!!URL::asset('js/jquery-ui.js')!!}"></script>
<script src="{!!URL::asset('selectableScroll.min.jsn.js')!!}"></script>
<script src="{!!URL::asset('js/jquery.ui.touch-punch.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/interviewer_dashboard.js')!!}"></script>
<script src="{!!URL::asset('js/star-rating.min.js')!!}"></script>


</body>


</html>