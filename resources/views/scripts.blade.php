{{--JQUERY UI IS partial and customised--}}
{{--core widget mouse accordion tabs draggable selectable--}}
<script type="text/javascript" src="{!!URL::asset('js/jquery.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/bootstrap.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/html5shiv.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/respond.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/jquery.easing.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/moment.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/bootstrapdatetimepicker.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/bootstrap-datepicker.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/jquery.timepicker.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/jquery.scrollTo.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/dentist.min.js') !!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/readmore.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/history.jquery.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/star-rating.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/jquery-ui.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/selectableScroll.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/jquery.ui.touch-punch.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/bootstrap-slider.min.js')!!}"></script>
<script type="text/javascript" src="{!!URL::asset('js/app.js')!!}"></script>