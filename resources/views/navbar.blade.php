<div class="header-fixed">
    <div class="header-content">
        <div class="header-topbar">
            <div class="clearfix header-brand">
                <a href="/" class="mi-text-center mi-font-11 logo-img-link fllt">
                    <img width="230px" height="38px" class="logo-img retina-img"
                         src="{!!URL::asset('/images/logo.png')!!}" alt="MockInterviews.com">
                </a>
            </div>
            <div class="search-unit">
                <div class="input-group search-input">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input id="query" type="text" class="form-control search-text-box"
                           placeholder="I am looking for ...">
                    <div class="input-group-btn">
                        <button id="search-btn" class="btn-default search-btn btn" type="submit">SEARCH</button>
                    </div>
                </div>
            </div>

            <div class="right-menu">
                <ul class="header-right-menu clearfix">
                    <li class="btn btn-default right-menu-yellow-btn">
                        @if(Request::is('shortlist'))
                            <a href="{{action('CompareController@compare')}}">Compare</a>
                        @else
                            <a href="{{action('ListingController@getCart')}}"><span>Shortlist</span></a>
                        @endif
                    </li>
                    @if(Auth::check())
                        <li class="btn btn-default right-menu-yellow-btn">
                            <a href="{{action('AuthenticationController@getDashboard')}}">{{ Auth::user()->first_name }}</a>
                        </li>
                        <li class="btn btn-default right-menu-yellow-btn">
                            <a href="{{action('AuthenticationController@getLogout')}}">Logout</a>
                        </li>
                    @else
                        <li class="btn btn-default right-menu-yellow-btn">
                            <a href="{{action('AuthenticationController@getLogin')}}">Login</a>
                        </li>
                        <li class="btn btn-default right-menu-yellow-btn">
                            <a href="{{action('AuthenticationController@getRegisterInterviewee')}}">Register</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>

        <?php $categories = App\Category::all()->toArray(); ?>
        <div class="header-second-bar">
            <ul class="header-menu clearfix mi-text-center">
                @for($i=0;$i<count($categories) && $i<9;$i++)
                    <li class="drop-menu drop-menu-item"
                        data-cling="{{ $categories[$i]['name'] }}">{{ $categories[$i]['name'] }}<span
                                class="caret"></span>
                    </li>
                @endfor
                @if(count($categories)>9)
                    <li class="drop-menu drop-menu-item" data-cling="more">More<span class="caret"></span></li>
                @endif

            </ul>
        </div>
        @for($i=0;$i<count($categories) && $i<10;$i++)
            <div class="dropdown-panel" id="{{ $categories[$i]['name'] }}">
                <h4>Subcategories under {{ $categories[$i]['name'] }}</h4>
                <hr/>
                <ul>
                    @foreach(App\SubCategory::where('category_id','=',$categories[$i]['id'])->get() as $subcategory)
                        <li><a class="dropdown-panel-link"
                               href="{{ action('ListingController@getListingPage') }}?subcategories={{ $subcategory->name  }}">{{$subcategory->name}}</a></li>
                    @endforeach
                </ul>
            </div>
        @endfor
        @if(count($categories)>9)
            <div class="dropdown-panel" id="more">
                <div class="panel-group" id="accordion">
                    @for($i=10;$i<count($categories);$i++)
                        <div class="panel panel-default">
                            <div class="panel-heading" data-toggle="collapse" data-parent="#accordion"
                                 href="#collapse{{$i}}">
                                <h5 class="panel-title">
                                    <a>{{ $categories[$i]['name'] }}<span class="caret"></span></a>
                                </h5>
                                <hr/>
                            </div>
                            <div id="collapse{{$i}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        @foreach(App\SubCategory::where('category_id','=',$categories[$i]['id'])->get() as $subcategory)
                                            <li><a class="dropdown-panel-link"
                                                   href="{{ action('ListingController@getListingPage') }}?subcategories={{ $subcategory->name  }}">{{$subcategory->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
        @endif
    </div>
</div>