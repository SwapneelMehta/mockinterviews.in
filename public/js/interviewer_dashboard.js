/**
 * Created by chirag on 26/07/15.
 */
jQuery.easing.easeOutQuart = function (x, t, b, c, d) {
    return -c * ((t = t / d - 1) * t * t * t - 1) + b;
};

var addAvailArray = [];

$(document).ready(function () {

    $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) { if($(this).attr('href')=='#tab_availability'){
        $('.adjust-margin-for-absolute').css('margin-left',$('.panel.panel-default.panel-left-absolute').width());
    } });


    var $body = $("body");
    $(document).on({
        ajaxStart: function () {
            $body.addClass("loading");
        },
        ajaxStop: function () {
            $body.removeClass("loading");
        }
    });

    $('#rating-input').rating({
        showClear: false,
        showCaption: false,
        step: 1
    });

    $('.add').click(function () {
        console.log($(this).attr('rating'));
        var cpy = $('#myModal');
        cpy.removeClass('hide');
        $(this).parent().parent().parent().append(cpy);
        $('#rating-input').rating('update', $(this).attr('rating'));
        if (!$(this).attr('rating')) {
            $('#rating-input').rating('update', 1);
        }
        $('#review').val($(this).attr('text'));
        $('#submitbtn').attr('for', $(this).attr('for'));
        $('#submitbtn').attr('by', $(this).attr('by'));
        $('#submitbtn').attr('listing', $(this).attr('listing'));
    });

    $('.upd').click(function () {
        console.log($(this).attr('rating'));
        var cpy = $('#myModal');
        cpy.removeClass('hide');
        $(this).parent().parent().parent().append(cpy);
        $('#rating-input').rating('update', $(this).attr('rating'));
        $('#review').val($(this).attr('text'));
        $('#submitbtn').attr('for', $(this).attr('for'));
        $('#submitbtn').attr('by', $(this).attr('by'));
        $('#submitbtn').attr('listing', $(this).attr('listing'));
    });

    $('#submitbtn').click(function (e) {

        e.preventDefault();
        var by = $(this).attr('by');
        var fr = $(this).attr('for');
        var listing = $(this).attr('listing');
        console.log(by + "" + fr + "" + listing);
        $.ajax({
            url: 'http://' + window.location.href.split('/')[2] + '/addReview',
            type: 'GET',
            data: {
                rating: $('#rating-input').rating()['0']['value'],
                text: $('#review').val(),
                by: by,
                for: fr,
                listing: listing
            },
            success: function (response) {
                if (response) {
                    if (response === "true") {
                        //console.log($('#listing'+listing));
                        $('#listing' + listing).text("Update Review");
                        $('#listing' + listing).addClass('upd');
                        $('#listing' + listing).removeClass('add');
                        $('#listing' + listing).attr('rating', $('#rating-input').rating()['0']['value']);
                        $('#listing' + listing).attr('text', $('#review').val());
                        $('#myModal').addClass('hide');
                    }
                }
            }
        });


    });

    $('#cancelbtn').click(function (e) {
        $('#myModal').addClass('hide');
    });

    $('.rating-container').attr('data-content', '★ ★ ★ ★ ★');
    $('.rating-stars').attr('data-content', '★ ★ ★ ★ ★');


    $('[data-toggle="tooltip"]').tooltip();


    $('div[id^="slideshow"]').selectableScroll({
        scrollSnapX: 5, // When the selection is that pixels near to the top/bottom edges, start to scroll
        scrollSnapY: 5, // When the selection is that pixels near to the side edges, start to scroll
        scrollAmount: 25, // In pixels
        scrollIntervalTime: 100, // In milliseconds
        tolerance: "touch",
        filter: ".timeslot",
        selected: function (event, ui) {
            var index = addAvailArray.indexOf($(ui.selected).attr('value'));
            if ($(ui.selected).hasClass('bg-green')) {
                if (index > -1) {
                    addAvailArray.splice(index, 1);
                }
                $(ui.selected).removeClass('bg-green');
                $(ui.selected).addClass('bg-red');
            }
            else if ($(ui.selected).hasClass('bg-red')) {
                if (index == -1) {
                    addAvailArray.push($(ui.selected).attr('value'));
                }
                $(ui.selected).removeClass('bg-red');
                $(ui.selected).addClass('bg-green');
            }
        }
    });


    $("#save-availabilities").on('click', function (e) {
        //make an ajax to save it all at once
        e.preventDefault();
        var availability_data = $('.timeslot.bg-green').map(function(){ return $(this).attr('value'); }).get().join();
        $.ajax({
            url: 'saveavailabilities',
            type: 'POST',
            data: {
                availability_post_data: availability_data
            },
            success: function (response) {

            }
        });
    });

});



