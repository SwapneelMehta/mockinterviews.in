var forgot_check = function () {
    var $forgot = $('#forgot');
    if (email_check($('#femail').val())) {
        $forgot.removeClass('disabled');
        $forgot.prop('disabled', false);
    } else {
        $forgot.addClass('disabled');
        $forgot.prop('disabled', true);
    }
};

$(document).ready(function () {
    $('#femail').keyup(forgot_check);
    $('#forgot').prop('disabled', true);
});