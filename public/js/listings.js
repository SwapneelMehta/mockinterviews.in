var current_page_number = 1;
var sortval = "price_asc";

//DOC READY
$(document).ready(function () {

    //Cache jquery selectors here
    var yearsofexperience_dom_cached = $("#yearsofexperience");
    var rating_dom_cached = $("#rating");
    var price_dom_cached = $("#price");
    var loadmore_dom_cached = $('#loadmore-row');
    var query_dom_cached = $('#query');
    var datetimepickerstart_dom_cached = $('#datetimepickerstart');
    var datetimepickerend_dom_cached = $('#datetimepickerend');
    var priceSlider;
    var ratingSlider;
    var yearsOfExperienceSlider;

    var History = window.History;// Note: We are using a capital H instead of a lower h
    History.Adapter.bind(window, 'statechange', function () { // Note: We are using statechange instead of popstate
        //console.log("History state change listner called");
        //getResultsViaUrl(History.getState().url, false);// Note: We are using History.getState() instead of event.state
    });

    $(document).on('click', '.panel-heading span.clickable', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    });

    var body_cached = $("body");
    $(document).on({
        ajaxStart: function () {
            body_cached.addClass("loading");
            loadmore_dom_cached.removeClass('hide');
        },
        ajaxStop: function () {
            body_cached.removeClass("loading");
        }
    });

    var collectFilters = function() {
        setHelpTexts();
        var companies = $('input:checkbox:checked.companies-option').map(function () {
            return this.value;
        }).get();
        companies = companies.join(';');

        var subcategories = $('input:checkbox:checked.subcategories-option').map(function () {
            return this.value;
        }).get();
        subcategories = subcategories.join(';');

        var certified = $('input:checkbox:checked.certified-option').map(function () {
            return this.value;
        }).get();
        certified = certified[0];

        var filterUrl = getBaseUrl() + '?';

        //fetch q=, textbox prefered over url
        if (query_dom_cached.val() == "CLEAR_SEARCH") {
            query_dom_cached.val("");
        }
        else if (query_dom_cached.val() != null && query_dom_cached.val() != "") {
            filterUrl = filterUrl + "q=" + query_dom_cached.val();
        }
        else {
            var tempUrl = getBaseUrl();
            if (window.location.href.indexOf('?') >= 0 && window.location.href.indexOf('?') < window.location.href.length - 1)
                tempUrl = tempUrl + window.location.href.substring(window.location.href.indexOf('?'));
            //if (!(window.location.hash == "" || window.location.hash == "#"))
            //    tempUrl = tempUrl + window.location.hash;
            var params = tempUrl.extract();
            if (params && params.q != null && params.q != "") {
                filterUrl = filterUrl + "q=" + params.q;
            }
        }

        filterUrl = filterUrl + "&minprice=" + priceSlider.getValue()[0] + "&maxprice=" + priceSlider.getValue()[1];
        filterUrl = filterUrl + "&rating=" + ratingSlider.getValue();
        filterUrl = filterUrl + "&yearsofexp=" + yearsOfExperienceSlider.getValue();
        if (certified == 'true') {
            filterUrl = filterUrl + "&certified=true";
        }
        if (companies && companies != "") {
            filterUrl = filterUrl + "&companies=" + companies;
        }
        if (subcategories && subcategories != "") {
            filterUrl = filterUrl + "&subcategories=" + subcategories;
        }
        if (datetimepickerstart_dom_cached.data("DateTimePicker").date() && datetimepickerend_dom_cached.data("DateTimePicker").date()) {
            filterUrl = filterUrl + "&startdate=" + datetimepickerstart_dom_cached.data("DateTimePicker").date().unix() + "&enddate=" + datetimepickerend_dom_cached.data("DateTimePicker").date().unix();
        }
        filterUrl = filterUrl + "&filter=yes";
        filterUrl = filterUrl + "&sortby=" + sortval;
        getResultsViaUrl(filterUrl, true, false);
    };

    //prepare sliders
    yearsofexperience_dom_cached.slider({min: 0, max: 10, value: 0});
    rating_dom_cached.slider({min: 0, max: 5, step: 0.5, value: 0});
    price_dom_cached.slider({});

    //prepare date time pickers
    datetimepickerstart_dom_cached.datetimepicker({
        format: 'DD/MM/YYYY h:mm a'
    });
    datetimepickerend_dom_cached.datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY h:mm a'
    });

    $(".dropdown-menu").on('click', 'li a', function () {
        $(".selected-sort-value").text($(this).text());
        sortval = $(this).attr('class');
        collectFilters();
    });

    priceSlider = price_dom_cached.on('slideStop', collectFilters).data('slider');
    ratingSlider = rating_dom_cached.on('slideStop', collectFilters).data('slider');
    yearsOfExperienceSlider = yearsofexperience_dom_cached.on('slideStop', collectFilters).data('slider');


    $('.subcategories-clear').click(function (e) {
        $('input:checkbox.subcategories-option').removeAttr('checked');
        collectFilters();
    });

    $('.companies-clear').click(function (e) {
        $('input:checkbox.companies-option').removeAttr('checked');
        collectFilters();
    });

    $('.search-clear').click(function (e) {
        query_dom_cached.val("CLEAR_SEARCH");
        collectFilters();
    });

    datetimepickerstart_dom_cached.on("dp.change", function (e) {

        datetimepickerend_dom_cached.data("DateTimePicker").minDate(e.date);

    });

    datetimepickerend_dom_cached.on("dp.change", function (e) {

        datetimepickerstart_dom_cached.data("DateTimePicker").maxDate(e.date);

    });

    datetimepickerend_dom_cached.on("dp.hide", function (e) {
        if (datetimepickerstart_dom_cached.data("DateTimePicker").date() !== null) {
            collectFilters();
        }
    });

    datetimepickerstart_dom_cached.on("dp.hide", function (e) {
        if (datetimepickerend_dom_cached.data("DateTimePicker").date() !== null) {
            collectFilters();
        }
    });

    function setHelpTexts() {
        $('#price_help').text('₹' + priceSlider.getValue()[0] + ' to ₹' + priceSlider.getValue()[1]);
        $('#rating_help').text(ratingSlider.getValue() + ' and above');
        $('#yearsofexperience_help').text(yearsOfExperienceSlider.getValue() + ' years and above');
    }

    //When Page is loaded via url or first time
    if (window.location.href.indexOf('?') == -1 && window.location.href.indexOf('#') == -1 && (window.location.hash == "" || window.location.hash == "#")) {
        //no params of any type
        //do load page 0 ajax
        getResultsViaUrl(window.location.href, true, true);
    }
    else if (window.location.href.indexOf('?') != -1 && (window.location.hash == "" || window.location.hash == "#")) {
        //found only ? params
        //call filter ajax
        getResultsViaUrl(window.location.href, true, true);
    }
    else if (window.location.href.indexOf('?') == -1 && window.location.href.indexOf('#') != -1 && (window.location.hash != "" || window.location.hash != "#")) {
        //found only # params
        //append them as get params
        //call filter ajax
        getResultsViaUrl(getBaseUrl() + '?' + window.location.hash.split('#')[1], true, true);
    }
    else if (window.location.href.indexOf('?') != -1 && window.location.href.indexOf('#') != -1 && (window.location.hash != "" || window.location.hash != "#")) {
        //found both
        //we apply the get params
        var finalurl = getBaseUrl() + '?' + window.location.href.substring(window.location.href.indexOf('?') + 1, window.location.href.indexOf(window.location.hash));
        getResultsViaUrl(finalurl, true, true);
        //Alternatively we can prefer hash params
        //var hashParams = window.location.hash.split('#')[1].extract();
        //var andParams = window.location.href.substring(window.location.href.indexOf('?')+1,window.location.href.indexOf(window.location.hash)).extract();
        //getResultsViaUrl(getBaseUrl() + '?' + window.location.hash.split('#')[1], true);
    }
    else {
        getResultsViaUrl(window.location.href, true, true);
    }

    $('.date-option,.certified-option,.companies-option,.subcategories-option').on('change', collectFilters);
    setHelpTexts();

    function getResultsViaUrl(url, pushFlag, setFiltersOnDOM) {
        var copyOfParamsToAppendAsHash = "";
        if (url.indexOf('?') != -1) {
            copyOfParamsToAppendAsHash = url.substring(url.indexOf('?') + 1);
        }
        current_page_number = 0;
        $.ajax({
            url: 'morelistings?' + copyOfParamsToAppendAsHash,
            type: 'GET',
            data: {
                page: current_page_number + 1
            },
            success: function (response) {
                if (response) {
                    //console.log("AJAX for results via url or collectfilters done");
                    $('.interview-row').remove();
                    if (response == "NOMORELISTINGS") {
                        loadmore_dom_cached.addClass('hide');
                    }
                    else {
                        current_page_number += 1;
                        loadmore_dom_cached.removeClass('hide');
                        $(response).insertBefore("#loadmore-row");
                    }
                    if (pushFlag) {
                        var finalurl = getBaseUrl() + '?' + copyOfParamsToAppendAsHash;
                        //console.log("Pushing " + finalurl);
                        History.pushState(null, null, finalurl);
                    }
                    //window.location.hash = copyOfParamsToAppendAsHash;
                    doDOMReadyTasks();
                    //console.log("Passed Do dom ready");
                    if (setFiltersOnDOM) {
                        //console.log("Set");
                        setFilters(url);
                        //console.log("Set");
                    }
                    var params = url.extract();
                    //console.log("extract");
                    var query_val = "";
                    if (params && params.q != null && params.q != "") {
                        query_val = params.q;
                        $('.query-text').text("\"" + query_val + "\"");
                        $('.query-results-div').removeClass('invisible');
                    }
                    else {
                        $('.query-results-div').addClass('invisible');
                    }

                    query_dom_cached.val(decodeURIComponent(query_val));

                }
            }
        });

    }

    //load more ajax
    $('#loadmore').click(function (e) {
        e.preventDefault();
        var url = window.location.href;
        $.ajax({
            url: 'morelistings?' + url.substring(url.indexOf('?') + 1),
            type: 'GET',
            data: {
                page: current_page_number + 1
            },
            success: function (response) {
                if (response) {
                    //console.log("Load more click event ajax done");
                    if (response == "NOMORELISTINGS") {
                        loadmore_dom_cached.addClass('hide');
                    }
                    else {
                        current_page_number += 1;
                        loadmore_dom_cached.removeClass('hide');
                        $(response).insertBefore("#loadmore-row");
                        doDOMReadyTasks();
                    }
                }
            }
        });
    });

    function setFilters(url) {
        var params = url.extract();
        //console.log("FGAYAVHGSHG");
        $('.certified-option').prop('checked', false);
        $('.companies-option').prop('checked', false);
        $('.subcategories-option').prop('checked', false);
        if (params && params.sortby) {
            $(".selected-sort-value").text($('.' + params.sortby).text());
            sortval = params.sortby;
        }
        else {
            $(".selected-sort-value").text('Price : Low to High');
        }
        //set Filters After Reload
        //set Price
        if (params && params.minprice && params.maxprice) {
            price_dom_cached.data('slider').setValue([parseInt(params.minprice), parseInt(params.maxprice)]);
            setHelpTexts();
        }

        //set Rating
        if (params && params.rating) {

            rating_dom_cached.data('slider').setValue(parseFloat(params.rating));
            setHelpTexts();
        }
        //set Companies
        if (params && params.companies) {
            var companiesArr = params.companies.split(';');
            for (var i = 0; i < companiesArr.length; i++)
                $('.companies-option[value="' + decodeURIComponent(companiesArr[i]) + '"]').prop('checked', true);

        }
        //set Subcategories
        if (params && params.subcategories) {
            var subcategoriesArr = params.subcategories.split(';');
            for (var j = 0; j < subcategoriesArr.length; j++) {
                $('.subcategories-option[value="' + decodeURIComponent(subcategoriesArr[j]) + '"]').prop('checked', true);
            }

        }
        //set years of exp
        if (params && params.yearsofexp) {
            yearsofexperience_dom_cached.data('slider').setValue(parseInt(params.yearsofexp));
            setHelpTexts();
        }
        //set certified
        if (params && params.certified) {
            $('.certified-option').prop('checked', true);
        }
        //set Date
        if (params && params.startdate && params.enddate) {
            datetimepickerstart_dom_cached.data("DateTimePicker").date(moment.unix(params.startdate));
            datetimepickerend_dom_cached.data("DateTimePicker").date(moment.unix(params.enddate));
        }
    }

    function doDOMReadyTasks() {
        $('[data-toggle="tooltip"]').tooltip();

        $('.shortlist-btn').hover(function () {
                var thisbtn = $(this);
            if (thisbtn.hasClass("shortlisted")) {
                thisbtn.css({"background-color":"#F34541","border":"1px solid #DA2C28","color":"white"});
                thisbtn.html("&nbsp;&nbsp;&nbsp;&nbsp;Remove&nbsp;&nbsp;&nbsp;&nbsp;");
            }
        }, function () {
            var thisbtn = $(this);
            if (thisbtn.hasClass("shortlisted")) {
                thisbtn.css({"background-color":"#fdd922","border":"1px solid #e0bc27","color":"#565656"});
                thisbtn.html("Shortlisted");
            }

        });

        $('.interviewer-summary').readmore({
            speed: 75,
            collapsedHeight: 80,
            moreLink: '<a href="#">(more)</a>',
            lessLink: '<a href="#">(less)</a>'
        });
        $('.shortlist-btn').on('click', function () {

            var thisbtn = $(this);
            if (thisbtn.hasClass("shortlist")) {
                $.ajax({
                    url: 'addtocart',
                    type: "POST",
                    data: {
                        value: $(this).attr('listing-id')
                    },
                    success: function (response) {
                        console.log(response);
                        if(response == "200") {
                            thisbtn.removeClass('shortlist').addClass('shortlisted');
                            thisbtn.text("Shortlisted");
                        }
                    }
                });
            }
            else if (thisbtn.hasClass("shortlisted")) {
                $.ajax({
                    url: 'removefromcart',
                    type: "POST",
                    data: {
                        value: $(this).attr('listing-id')
                    },
                    success: function () {
                        thisbtn.removeClass('shortlisted').addClass('shortlist');
                        thisbtn.css({"background-color":"#fdd922","border":"1px solid #e0bc27","color":"#565656"});
                        thisbtn.text("Shortlist");
                    }
                });
            }
        });

    }

});
