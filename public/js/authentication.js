/**
 * Created by paren on 9/16/2015.
 */
function email_check (email) {
    var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return email!=null && email!=undefined && email!="" && re.test(email);
}

function pass_check (password) {

    var error = "Your password must ";
    var e = 0;
    if(password.length < 8){
        if(e==0 || e==1) {
            error += "be at least 8 characters long";
            e = 1;
        }
    }
    if(password.search(/[a-z]/i)<0){
        if(e==0){
            error += "contain at least one alphabet";
            e = 2;
        }
        else{
            error += ", contain at least one alphabet";
            e = 3;
        }
    }
    if(password.search(/\d/)<0){
        if(e==0){
            error += "contain at least one digit";
            e=4;
        }
        else if(e==1){
            error += ", contain at least one digit";
        }
        else if(e==2||e==3){
            error += "and one digit";
        }
    }
    if(e==0 && password!=null && password!=undefined && password!=""){
        return 0;
    }
    else{
        return error;
    }
}

function dob_check (dob) {
    return dob!=null && dob!=undefined && dob!="" && /^\d\d\/\d\d\/\d\d\d\d$/.test(dob);
}

function city_check (city) {
    return (city!='--------');
}

function phone_check (phone) {
    return phone!=null && phone!=undefined && phone!="" && /^\+?[0-9]{10,12}$/.test(phone);
}

function name_check (name) {
    var re = /^[a-zA-Z ]+$/;
    return name!=null && name!=undefined && name!="" && (re.test(name));
}

function bio_check (bio) {
    return bio!=null && bio!=undefined && bio!="" && /^.{140,5000}$/.test(bio);
}