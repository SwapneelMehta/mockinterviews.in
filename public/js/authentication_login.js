$(document).ready(function () {
    var $redirect = $('#redirect');
    $('[data-toggle="tooltip"]').tooltip();
    var url = window.location.href;
    var params = url.extract();
    if (params && params.redirect) {
        $redirect.prop('name', 'redirect');
        $redirect.prop('value', params.redirect);
    }

    $('#login').click(function () {
        $("body").addClass('loading');
        $(".loading_modal").css('display', 'block');
        $('#login_form').submit();
    });

    $('.btn-google,.btn-linkedin').click(function () {
        $("body").addClass('loading');
        $(".loading_modal").css('display', 'block');
    });
});