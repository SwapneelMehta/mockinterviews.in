<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class fakedata extends Seeder
{
    public function run()
    {
        //Truncater
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        $tables = DB::select('SHOW TABLES');
        $Tables_in_database = "Tables_in_mockinterviews";
        foreach ($tables as $table) {
            if ($table->$Tables_in_database !== 'migrations' || $table->$Tables_in_database !== 'administrators')
                DB::table($table->$Tables_in_database)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        //Skills gen
        $cats = ['I.T.', 'Management', 'Design'];
        $subcats = [['System Administration', 'S.E.O.', 'Software Engineer', 'Android', 'iOS', 'Windows Phone Development', 'FrontEnd Development', 'Django', 'Laravel', 'Flask'],
            ['H.R.', 'Finance', 'Engineering Management'],
            ['Maya', 'Blenders', 'Photoshop', 'Illustrator', 'Protoyping']];
        $subcatsidsarray = [];
        for ($i = 0; $i < sizeof($cats); $i++) {
            $catid = DB::table('categories')->insertGetId([
                'name' => $cats[$i]
            ]);
            $tempsubcatidsarray = [];
            for ($j = 0; $j < sizeof($subcats[$i]); $j++) {
                $subcatid = DB::table('subcategories')->insertGetId([
                    'name' => $subcats[$i][$j],
                    'category_id' => $catid
                ]);
                array_push($tempsubcatidsarray, $subcatid);
            }
            array_push($subcatsidsarray, $tempsubcatidsarray);
        }

        //Faker start
        $faker = Faker::create();


        //interviewers
        foreach (range(1, 20) as $index) {
//            $id = DB::table('social_auths')->insertGetId([
//                'google_access_token' => $faker->unique()->numerify('##########'),
//                'linkedin_access_token' => $faker->unique()->numerify('##########')
//            ]);

            $uid = DB::table('users')->insertGetId([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->email,
                'password' => Hash::make('Engineer$@987'),
                'activated' => true,
                'phone_no' => $faker->unique()->phoneNumber,
                'city' => $faker->city,
                'bio' => $faker->paragraph,
                'dob' => $faker->dateTimeBetween('-40 years', '-20 years')
            ]);

            $irid = DB::table('interviewers')->insertGetId([
                'user_id' => $uid,
                'verified' => $faker->boolean()
            ]);

            foreach (range(1, $faker->numberBetween(1, 4)) as $i) {
                DB::table('experiences')->insertGetId([
                    'company_name' => $faker->company,
                    'location' => $faker->city,
                    'description' => $faker->paragraph,
                    'title' => $faker->userName,
                    'start_date' => $faker->dateTimeBetween('-20 years', '-10 years'),
                    'end_date' => $faker->dateTimeBetween('-15 years', '-10 years'),
                    'user_id' => $uid
                ]);
            }

            foreach ([1, 2] as $i) {
                DB::table('educations')->insertGetId([
                    'institution' => "University of " . $faker->city,
                    'degree' => $faker->randomElement(['Masters', 'Bacheleors', 'PhD']) . " in " . $faker->randomElement(['Science', 'Engineering', 'Commerce', 'Management']) . "(" . $faker->randomElement(['C.S.', 'I.T.', 'Finance', 'Physics', 'Electronics', 'Human Resources', 'Design']) . ")",
                    'start_date' => $faker->dateTimeBetween('-25 years', '-20 years'),
                    'end_date' => $faker->dateTimeBetween('-20 years', '-15 years'),
                    'user_id' => $uid
                ]);
            }

            foreach ($faker->randomElements($subcatsidsarray[$faker->numberBetween(0, 2)], $faker->numberBetween(1, 3)) as $subcatid) {
                DB::table('user_subcategory')->insertGetId([
                    'subcategory_id' => $subcatid,
                    'user_id' => $uid
                ]);

                DB::table('listings')->insertGetId([
                    'price' => $faker->numberBetween(100, 10000),
                    'description' => $faker->paragraph(),
                    'interviewer_id' => $irid,
                    'subcategory_id' => $subcatid
                ]);
            }

            //availabilities
            $startdate = \Carbon\Carbon::create(2016, 10, 14, 9, 0, 0);
            $enddate = $startdate->copy()->addDays(30);
            while ($startdate < $enddate) {
                $starttime = $startdate->copy();
                $endtime = $startdate->copy()->addHours(11);
                while ($starttime < $endtime) {

                    if ($faker->boolean(70)) {
                        DB::table('availabilities')->insertGetId([
                            'start_timestamp' => $starttime->toDateTimeString(),
                            'end_timestamp' => $starttime->copy()->addHour()->toDateTimeString(),
                            'interviewer_id' => $irid
                        ]);
                    }

                    $starttime->addHour();
                }
                $startdate->addDay();
            }
        }

        //interviewees
        foreach (range(1, 20) as $index) {
//            $id = DB::table('social_auths')->insertGetId([
//                'google_access_token' => $faker->unique()->numerify('##########'),
//                'linkedin_access_token' => $faker->unique()->numerify('##########')
//            ]);

            $uid = DB::table('users')->insertGetId([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->email,
                'password' => Hash::make('Engineer$@987'),
                'activated' => true,
                'phone_no' => $faker->unique()->phoneNumber,
                'city' => $faker->city,
                'bio' => $faker->paragraph,
                'dob' => $faker->dateTimeBetween('-50 years', '-16 years')
            ]);

            $ieid = DB::table('interviewees')->insertGetId([
                'user_id' => $uid
            ]);

            if ($faker->boolean()) {
                DB::table('experiences')->insertGetId([
                    'company_name' => $faker->company,
                    'location' => $faker->city,
                    'description' => $faker->paragraph,
                    'title' => $faker->userName,
                    'start_date' => $faker->dateTimeBetween('-15 years', '-10 years'),
                    'end_date' => $faker->dateTimeBetween('-10 years', 'now'),
                    'user_id' => $uid
                ]);
            }

            foreach ([1, 2] as $i) {
                DB::table('educations')->insertGetId([
                    'institution' => "University of " . $faker->city,
                    'degree' => $faker->randomElement(['Masters', 'Bacheleors', 'PhD']) . " in " . $faker->randomElement(['Science', 'Engineering', 'Commerce', 'Management']) . "(" . $faker->randomElement(['C.S.', 'I.T.', 'Finance', 'Physics', 'Electronics', 'Human Resources']) . ")",
                    'start_date' => $faker->dateTimeBetween('-25 years', '-20 years'),
                    'end_date' => $faker->dateTimeBetween('-20 years', '-15 years'),
                    'user_id' => $uid
                ]);
            }

            foreach ($faker->randomElements($subcatsidsarray[$faker->numberBetween(0, 2)], $faker->numberBetween(1, 3)) as $subcatid) {
                DB::table('user_subcategory')->insertGetId([
                    'subcategory_id' => $subcatid,
                    'user_id' => $uid
                ]);
            }

        }
    }
}
