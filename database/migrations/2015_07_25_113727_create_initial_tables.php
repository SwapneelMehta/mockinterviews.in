<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 50)->nullable();
            $table->string('middle_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->boolean('activated')->default(false);
            $table->string('email', 50)->unique();
            $table->string('phone_no', 13)->nullable();
            $table->string('password', 64)->nullable();
            $table->rememberToken();
            $table->string('city', 15)->nullable();
            $table->text('bio')->nullable();
            $table->date('dob')->nullable();
            $table->string('gender',1)->default('O');
            $table->text('google_access_token')->nullable();
            $table->text('linkedin_access_token')->nullable();
            $table->string('resume_url')->nullable();
            $table->string('profile_image_url')->nullable()->default('0_profile_image.jpg');
            $table->timestamps();
        });

        Schema::create('interviewers', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('verified');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade');

            $table->timestamps();
        });

        Schema::create('interviewees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('interviewers');
        Schema::drop('interviewees');
        Schema::drop('users');

    }
}
