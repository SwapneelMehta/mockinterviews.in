<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appointment_id')->unsigned();
            $table->foreign('appointment_id')
                ->references('id')->on('appointments')
                ->onUpdate('cascade');
            $table->string('offer_type', 50)->nullable();
            $table->integer('coupon_id')->unsigned()->nullable();
            $table->foreign('coupon_id')
                ->references('id')->on('coupons')
                ->onUpdate('cascade');
            $table->double('amount', 15, 8)->unsigned();
            $table->string('payment_id', 50);
            $table->string('tracking_id', 50);
            $table->string('payment_mode', 50);
            $table->string('bank_ref_no', 50)->nullable();
            $table->string('card_number', 50)->nullable();
            $table->double('discount_value', 15, 8)->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
