<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class SubCategory extends SleepingOwlModel
{
    //
    protected $table = 'subcategories';
    protected $fillable = ['name','category_id','skilledUsers'];
    protected $with = ['category'];

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }

    public function skilledUsers(){
        return $this->belongsToMany('App\User','user_subcategory', 'subcategory_id', 'user_id');
    }

    public function listings(){
        return $this->hasMany('App\Listing','subcategory_id');
    }

    public static function getList(){
        $data = array();
        foreach (SubCategory::all() as $i)
        {
            $data += array($i->id => $i->name);
        }
        return $data;

    }

    public function setSkilledUsersAttribute($skilledUsers)
    {
        $this->skilledUsers()->detach();
        if ( ! $skilledUsers) return;
        if ( ! $this->exists) $this->save();
        $this->skilledUsers()->attach($skilledUsers);
    }
}

