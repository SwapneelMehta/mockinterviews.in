<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class Order extends SleepingOwlModel
{
    //
    protected $table    = 'orders';
    protected $fillable = ['appointment_id', 'offer_type', 'coupon_id', 'amount', 'payment_id', 'tracking_id', 'payment_mode', 'bank_ref_no', 'card_number', 'discount_value'];

    public function appointment(){
        return $this->belongsTo('App\Appointment','appointment_id');
    }

    public function coupon(){
        return $this->belongsTo('App\Coupon','coupon_id');
    }

}
