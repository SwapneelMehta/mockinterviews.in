<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class Category extends SleepingOwlModel
{
    //
    protected $table = 'categories';
    protected $fillable = ['name'];

    public function subcategories(){
        return $this->hasMany('App\SubCategory','category_id');
    }

    public static function getList(){
        $data = array();
        foreach (Category::all() as $i)
        {
            $data += array($i->id => $i->name);
        }
        return $data;

    }
}


