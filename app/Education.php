<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class Education extends SleepingOwlModel
{
    //
    protected $table = 'educations';
    protected $fillable = ['user_id','institution','degree','start_date','end_date'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function getDates()
    {
        return array_merge(parent::getDates(), ['start_date','end_date']);
    }
}