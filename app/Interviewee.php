<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class Interviewee extends SleepingOwlModel
{
    //
    protected $table = 'interviewees';
    protected $fillable = ['user_id','coupons','couponPairs'];
    protected $with = ['user'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function appointments(){
        return $this->hasMany('App\Appointment','interviewee_id');
    }

    public function coupons(){
        return $this->belongsToMany('App\Coupon','coupon_interviewee', 'interviewee_id', 'coupon_id');
    }

    public function couponPairs(){
        return $this->hasMany('App\Coupon_Interviewee','interviewee_id');
    }


    public static function getList(){
        $data = array();
        foreach (Interviewee::all() as $i)
        {
            $data += array($i->id => $i->user->first_name.' '.$i->user->last_name);
        }
        return $data;

    }

    public function setCouponsAttribute($coupons)
    {
        $this->coupons()->detach();
        if ( ! $coupons) return;
        if ( ! $this->exists) $this->save();
        $this->coupons()->attach($coupons);
    }
}
