<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;
use Carbon\Carbon;
class Availability extends SleepingOwlModel
{
    //
    protected $table = 'availabilities';
    protected $fillable = ['start_timestamp','end_timestamp','interviewer_id'];
    protected $with = ['interviewer'];

    public function interviewer(){
        return $this->belongsTo('App\Interviewer','interviewer_id');
    }

    public function appointment(){
        return $this->hasOne('App\Appointment','availability_id');
    }

    public static function getList(){
        $data = array();
        foreach (Availability::where('start_timestamp','>',Carbon::now())->get() as $i)
        {
            $data += array($i->id => $i->interviewer->user->first_name.' '.$i->start_timestamp.' to '.$i->end_timestamp);
        }
        return $data;

    }
}