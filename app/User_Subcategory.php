<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Subcategory extends Model
{
    //
    protected $table = 'user_subcategory';
    protected $fillable = ['user_id','subcategory_id', 'name'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function subcategory(){
        return $this->belongsTo('App\SubCategory','subcategory_id');
    }

}
