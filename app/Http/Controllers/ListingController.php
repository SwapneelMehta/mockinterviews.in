<?php

namespace App\Http\Controllers;

use App\Experience;
use App\Interviewer;
use App\Listing;
use App\SubCategory;
use App\User_Subcategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Log;
use \Session;
use App\Review;
use App\Interviewee;

class ListingController extends Controller
{
    public function getMoreListings(Request $request){
        if ($request->ajax()) {
            $perPage = 10;
            $filterObject = new FilterController();
            $q = Input::get('q', -1);
            $minprice = Input::get('minprice', -1);
            $maxprice = Input::get('maxprice', -1);
            $certified = Input::get('certified', -1);
            $subcategories = explode(';', Input::get('subcategories', ''));
            $startdate = Input::get('startdate', -1);
            $enddate = Input::get('enddate', -1);
            $yearsofexp = Input::get('yearsofexp', -1);
            $companies = explode(';', Input::get('companies', ''));
            $rating = Input::get('rating', -1);
            $results = Listing::getOnlyAvailableListings();
            if ($q != -1 && $q != "")
                $results = $filterObject->getResult($q);
            if ($minprice != -1)
                $results = $filterObject->filterPrice($minprice, $maxprice, $results);
            if ($certified != -1)
                $results = $filterObject->filterCert($results);
            if (count($subcategories) > 0 && $subcategories[0] != '')
                $results = $filterObject->filterCat($subcategories, $results);
            if ($startdate != -1)
                $results = $filterObject->filterDate($startdate, $enddate, $results);
            if ($yearsofexp != -1)
                $results = $filterObject->filterYears($yearsofexp, $results);
            if ($rating != -1)
                $results = $filterObject->filterRating($rating, $results);
            if (count($companies) > 0 && $companies[0] != '')
                $results = $filterObject->filterCompany($companies, $results);


            $listings = array();
            $results = $results->sortBy(function ($listing) {
                return $listing->price;
            });
            $sortval = Input::get('sortby', -1);
            if ($sortval != -1) {
                if ($sortval == "price_desc")
                    $results = $results->sortByDesc(function ($listing) {
                        return $listing->price;
                    });
                elseif ($sortval == "rating_asc")
                    $results = $results->sortBy(function ($listing) {
                        return $listing->interviewer->avgRating();
                    });
                elseif ($sortval == "rating_desc")
                    $results = $results->sortByDesc(function ($listing) {
                        return $listing->interviewer->avgRating();
                    });
                elseif ($sortval == "yearsofexp_asc")
                    $results = $results->sortBy(function ($listing) {
                        return $listing->interviewer->yearsOfExperience() / 12;
                    });
                elseif ($sortval == "yearsofexp_desc")
                    $results = $results->sortByDesc(function ($listing) {
                        return $listing->interviewer->yearsOfExperience() / 12;
                    });
                elseif ($sortval == "date_asc")
                    $results = $results->sortBy(function ($listing) {
                        strtotime($listing->interviewer->availabilities->sortBy('start_timestamp')->first()->start_timestamp);
                    });
                elseif ($sortval == "date_desc")
                    $results = $results->sortByDesc(function ($listing) {
                        strtotime($listing->interviewer->availabilities->sortBy('start_timestamp')->first()->start_timestamp);
                    });
            }
            $listings = $results->toArray();
            $paginator = new Paginator($listings);
            return $this->makeHtml($paginator->paginate($perPage));
        }
        else
        {
            return response('This API ain\'t open!', 503);
        }
    }

    public function getListingPage(Request $request){
        if(!self::isMinimumProfileComplete()){
            return redirect(self::$minimumProfileCompletionRedirectPath);
        }
        $companies = Experience::select('company_name')->distinct('company_name')->get();
        $subcategories = SubCategory::all();
        $maxprice = Listing::max('price');
        return view('main.listings',compact('companies','subcategories','maxprice'));
    }

    //TODO
    public function getCart(){
        if(!self::isMinimumProfileComplete()){
            return redirect(self::$minimumProfileCompletionRedirectPath);
        }
        $cart_ids = Session::get('cart_ids', -1);
        $listings = new Collection();
        if($cart_ids!=-1)
            $listings = Listing::whereIn('id', array_values($cart_ids))->get();
        return view('main.cart', compact('listings'));
    }

    //TODO
    public function unsetCart(Request $request){
        Session::put('cart_ids', array_values(array_diff(Session::get('cart_ids'), [$request['value']])));
        return sizeof(Session::get('cart_ids'));
    }

    //TODO
    public function editInterviewerProfile(Request $request,$id){
        $interviewer = Interviewer::where('id','=',$id)->first();
        $skillset = User_Subcategory::all();
        $subcats = SubCategory::all();
        return view('main.interviewer_edit',compact('interviewer','skillset','subcats'));
    }

    public function editIntervieweeProfile(Request $request,$id){
        $interviewer = Interviewee::where('id','=',$id)->first();
        $skillset = User_Subcategory::all();
        $subcats = SubCategory::all();
        return view('main.interviewee_edit',compact('interviewer','skillset','subcats'));
    }

    //TODO Check
    public function addReview(Request $request){
        Log::error($request['for'].$request['by'].$request['listing']);
        $reviews = Review::all();
        $found = 0;
        $foundrev = "";
        foreach($reviews as $rev){
            if($rev->listing_id == $request['listing'] && $rev->by_user_id == $request['by'] && $rev->for_user_id == $request['for']){
                $found = 1;
                $foundrev = $rev;
                break;
            }
        }
        if($found===1){
            $foundrev->rating = $request['rating'];
            $foundrev->text = $request['text'];
            $foundrev->save();

        }
        else{
            $foundrev = new Review;
            $foundrev->rating = $request['rating'];
            $foundrev->text = $request['text'];
            $foundrev->for_user_id = $request['for'];
            $foundrev->by_user_id = $request['by'];
            $foundrev->listing_id = $request['listing'];
            $foundrev->save();

        }
        return "true";
    }

    //TODO
    public function saveEdit(Request $request){

    }

    public function makeHtml($results){
        $htmlString = "";
        $j=0;
        $diff=0;
        $shortlisted = Session::get('cart_ids');
        $user_type = Session::get('type');
        if($shortlisted) {
            $shortlisted=array_values($shortlisted);
        }
        else {
            $shortlisted = array();
        }
        foreach($results as $listing)
        {
            $html = "";
            $r=0;
            $photo = $listing['interviewer']['user']['photo'];
            $interviewer_id = $listing['interviewer']['id'];
            $interviewer_name = $listing['interviewer']['user']['first_name'].' '.$listing['interviewer']['user']['last_name'];
            $verified = "";
            if($listing['interviewer']['verified'] == 1)
                $verified.='<i class="fa fa-check-circle" data-toggle="tooltip" data-placement="bottom" title="Certified Interviewer"></i>';
            $rating = "";
            for($j=1;$j<=intval($listing['interviewer']['rating']);$j=$j+1)
                $rating.='<span class="fa fa-star"></span>';
            $diff = $listing['interviewer']['rating'] - intval($listing['interviewer']['rating']);
            if($diff>=0.25 && $diff<=0.75) {
                $rating .= '<span class="fa fa-star-half-o"></span>';
                $r = 1;
            }
            for($j=intval($listing['interviewer']['rating'])+$r;$j<5;$j=$j+1)
                $rating.='<span class="fa fa-star-o"></span>';
            $price = $listing['price'];
            $yoe="";
            if($listing['interviewer']['yearsofexperience'] != 0)
                $yoe.='<div class="row interviewer-current-work"><div>'.intval($listing['interviewer']['yearsofexperience']/12).'+ Years of Experience</div></div>';
            $subcategory = $listing['subcategory']['name'];
            $description = $listing['description'];
            $listing_id = $listing['id'];
            if(in_array($listing_id,$shortlisted)) {
                $shortlist_status = "Shortlisted";
                $shortlist_class = "shortlisted";
            }
            else {
                $shortlist_status = "Shortlist";
                $shortlist_class = "shortlist";
            }
            $shortlist_div = '<div class="btn-default shortlist-btn btn pull-right '.$shortlist_class.'" listing-id="'.$listing_id.'">'.$shortlist_status.'</div>';
            if($user_type == 'interviewer')
                $shortlist_div = "";
            $html = <<<MAKEHTML
            <div class="row interview-row">
                <div class="interviewer-img col-xs-2">
                    <img src="$photo">
                </div>
                <div class="interview-details col-xs-10">
                    <div class="row">
                        <div class="interviewer-name">
                            <a class="black-link" href="interviewer/profile/$interviewer_id">
                                $interviewer_name
                            </a>
                            $verified
                        </div>
                        <div class="star-rating">
                            $rating
                        </div>
                        <div class="pull-right">
                            $shortlist_div
                            <div class="interview-price pull-right">
                                &#8377;$price
                            </div>
                        </div>
                        $yoe
                        <div class="row interviewer-current-work">
                            <div>Interview for $subcategory</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="interviewer-summary">
                            $description
                        </div>
                    </div>
                </div>
            </div>
MAKEHTML;
            $htmlString = $htmlString.$html;
        }
        if($htmlString=="")
            return "NOMORELISTINGS";
        return $htmlString;
    }

}