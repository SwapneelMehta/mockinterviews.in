<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use View;

class AdminController extends Controller
{
    public function getIndex()
    {
        return view('admin.start');
    }
}