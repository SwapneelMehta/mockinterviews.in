<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App;
use App\User;
use App\Availability;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Interviewee;
use App\Interviewer;
use Intervention\Image\ImageManager;
use \Mail;
use Log;
use Mockery\CountValidator\Exception;
use \URL;
use Carbon\Carbon;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{

    public function getForcePasswordChange(Request $request){
        return view('auth.forcepass')->with('userid', $request['id']);
    }

    public function postForcePassword(Request $request){
        $user = User::where('id', $request['userid'])->first();
        if ($request['current'] == $user['password']){
            $user['password'] = Hash::make($request['new']);
            $user->save();
            return redirect('login')->with('status', 'Password Changed successfully!');
        } else{
            return redirect()->back()->with('status', 'Old Password Does Not Match the Password Set By Admin');
        }

    }

    public function getLogin(){
        if(Auth::check()){
            return redirect()->back();
        }
        return view('auth.login');
    }

    public function postLogin(Request $request){
        $user = User::where('email', $request['email'])->first();
        if ($request['redirect']){
            $redirect = urldecode($request['redirect']);
        } else{
            $redirect = '/';
        }
        if ($user){
            if ($user->activated){
                if(Auth::viaRemember() || ($user['password']!==null && Auth::attempt(['email' => $request['email'],'password' => $request['password']],$request['remember_me']))){
                    $interviewee = Interviewee::where('user_id', $user['id'])->first();
                    $interviewer = Interviewer::where('user_id', $user['id'])->first();
                    if ($interviewee){
                        Session::put('type', 'interviewee');
                    }
                    if ($interviewer){
                        Session::put('type', 'interviewer');
                        Session::forget('cart_ids');
                    }
                    return redirect($redirect);
                }
                else if($user['password'] == $request['password']){
                    // Redirect to change password page
                    $url = action('AuthenticationController@getForcePasswordChange', ['id' => $user['id']]);
                    return redirect($url);
                    //return redirect('register');
                }
                else{
                    return redirect()->back()->withInput()->with('status', 'Invalid Email or Password');
                }
            }
            else{
                return redirect()->back()->withInput()->with('status', 'Please Check Your Mail for Activation Link');
            }
        }
        return redirect()->back()->withInput()->with('status', 'Invalid Email or Password');
    }

    public function getRegisterInterviewee(Request $request){

        if (Auth::check()){
            return redirect()->back();
        }
        return view('auth.register_interviewee');
    }

    public function getRegisterInterviewer(Request $request){

        if (Auth::check()){
            return redirect()->back();
        }
        return view('auth.register_interviewer');
    }

    public function postRegister(Request $request){
        $users = User::where('email', $request['email'])->first();
        if ($users) {
            return redirect()->back()->withInput()->with('status', 'Already Registered User');
        } else {
            $user = User::create(['email' => $request['email'],
                'first_name' => $request['fname'], 'last_name' => $request['lname'],
                'password' => Hash::make($request['password']), 'profile_image_url' => '0_profile_image.jpg',
            ]);
            try {
                $hash = strrev(md5($user['password']));
                $url = action('AuthenticationController@getActivation', ['email' => $user['email'], 'hash' => $hash]);
                $str = "Hi,\nPlease Visit the following link to activate your account\n" . $url;
                Mail::raw($str, function ($message) use ($user) {
                    $message->from('noreply@mockinterviews.in', 'Mockinterviews.in');
                    $message->to($user['email'], $user['fname'] . ' ' . $user['lname'])->subject('Welcome to Mockinterviews');
                });
                $user->save();
                if ($request['type'] == 'interviewee') {
                    Interviewee::create(['user_id' => $user['id']]);
                } else if ($request['type'] == 'intervieweer') {
                    Interviewer::create(['user_id' => $user['id']]);
                } else {
                    throw new Exception();
                }
                return redirect('login')->with('status', 'Successfully registered! An account activation link has been sent to your email');;
            } catch (Exception $e) {
                Log::error("catch mail");
                $user->delete();
                return redirect()->back()->withInput()->with('status', 'Something went wrong.Please Try Again');

            }
        }
    }

    public function getActivation(Request $request){
        $user = User::where('email', $request['email'])->first();
        if ($user['activated'] == false && md5($user['password']) == strrev($request['hash'])){
            $user->activated = true;
            $user->save();
            return redirect('login')->with('status', 'Successfully activated');
        }
        return redirect('login');
    }

    public function getLogout(){
        Session::flush();
        Auth::logout();
        return redirect('/');
    }

    public function getForgot(){
        if (Auth::check()){
            return redirect()->back();
        }
        return view('auth.forgot');
    }

    public function postForgot(Request $request){
        $user = User::where('email', $request['email'])->first();
        if ($user){
            $url = action('AuthenticationController@getForgotLink', ['email' => $user['email'], 'hash' => md5($user['password'])]);
            $str = "Hi " . $user['first_name'] . ",\nYou requested that your password be reset.\nIf you didn't make this request then ignore this email,no changes have been made.\nIf you did request a password reset,please visit the following link to recover your account\n" . $url;
            Mail::raw($str, function ($message) use ($user){
                $message->from('noreply@mockinterviews.in', 'Mockinterviews.in');
                $message->to($user['email'], $user['fname'] . ' ' . $user['lanme'])->subject('Mockinterviews, Forgot Password Link');
            });
            return redirect('login')->with('status', 'A password reset link has been sent to your email');
        }
        return view('auth.forgot')->with('status', 'Account not found');
    }

    public function getForgotLink(Request $request){
        if(Auth::check()){
            return redirect()->back();
        }
        return view('auth.changepass');
    }

    public function postForgotLink(Request $request){
        $user = User::where('email', $request['userid'])->first();
        if($request['hash'] == md5($user['password'])){
            $user->password = Hash::make($request['new']);
            $user->save();
            return redirect('login')->with('status', 'Password Changed Successfully');
        } else{
            return redirect('forgot_password')->with('status', 'Link is Invalid');
        }

    }

    public function setSession(Request $request){
        $a = (Session::get('cart_ids'));
        if ($a){
            $s = array_values($a);
        }
        else{
            $s = array();
        }
        if (!in_array($request['value'], $s, true)){
            Session::push('cart_ids', $request['value']);
        }
        return '200';
    }

    public function sendContactMail(Request $request){
        $str = "From : " . $request['name'] . "\nMail : " . $request['email'] . "\nPhone No : " . $request['phone'] . "\nMessage : " . $request['message'];
        Mail::raw($str, function ($message) use ($request){
            $message->from('noreply@mockinterviews.in', 'Mockinterviews.in');
            $message->to('contact@mockinterviews.in', 'Contact Us')->subject('Query email');
        });
        return redirect()->back()->with('status', 'Something went wrong.Please Try Again');
    }

    public function getAbout(Request $request){
        return view("about");
    }

    public function getDashboard(Request $request){
        if (Auth::check() && Session::get('type') == 'interviewer'){
            return $this->getInterviewerProfile($request, Interviewer::where('user_id', '=', Auth::user()->id)->first()->id);
        } elseif (Auth::check() && Session::get('type') == 'interviewee'){
            return $this->getIntervieweeProfile($request, Interviewee::where('user_id', '=', Auth::user()->id)->first()->id);
        } else{
            return redirect('/');
        }
    }

    public function getInterviewerProfile(Request $request, $id){
        if(!self::isMinimumProfileComplete()){
            return redirect(self::$minimumProfileCompletionRedirectPath);
        }
        $interviewer = Interviewer::where('id', '=', $id)->first();
        $availabilites_as_unix_timestamp = array();
        $appointments_as_unix_timestamp = array();
        $availabilites = $interviewer->availabilitiesNotAppointments();
        $appointments = $interviewer->appointments();
        foreach ($availabilites as $availability){
            $availabilites_as_unix_timestamp[Carbon::parse($availability->start_timestamp)->timestamp] = true;
        }
        foreach ($appointments as $appointment){
            $appointments_as_unix_timestamp[Carbon::parse($appointment->availability->start_timestamp)->timestamp] = true;
        }
        return view('main.interviewer_dashboard', compact('interviewer', 'availabilites_as_unix_timestamp', 'appointments_as_unix_timestamp'));

    }

    public function getIntervieweeProfile(Request $request, $id){
        if(!self::isMinimumProfileComplete()){
            return redirect(self::$minimumProfileCompletionRedirectPath);
        }
        $interviewer = Interviewee::where('id', '=', $id)->first();
        return view('main.interviewee_dashboard', compact('interviewer'));
    }

    public function redirectToProvider($register_type, $provider){
        Session::put('register_type', $register_type);
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider){
        try {
            $user = Socialite::driver($provider)->user();
        }
        catch(\Exception $e){
            return redirect('/login')->with('status','Access denied for '.$provider);
        }
        $data = [
            'first_name' => null,
            'last_name' => null,
            'email' => null,
            'google_access_token' => null,
            'linkedin_access_token' => null,
            'activated' => true,
        ];
        if($provider=='google'){
            $data['first_name'] = $user['name']['givenName'];
            $data['last_name'] = $user['name']['familyName'];
            $data['email'] = $user->getEmail();
            $data['google_access_token'] = $user->token;
        }
        else if($provider=='linkedin'){
            $data['first_name'] = $user['firstName'];
            $data['last_name'] = $user['lastName'];
            $data['email'] = $user->getEmail();
            $data['linkedin_access_token'] = $user->token;
        }
        $register_type = Session::get('register_type');
        if($data['email']!=null && ($register_type=='interviewer' || $register_type=='interviewee' || $register_type=='login')){
            $user=User::where('email', $data['email'])->first();
            if(!$user &&  $register_type=='login'){
                return redirect('login')->with('status','Please register first');
            }
            if($user){
                if ($provider=='google'){
                    $user->google_access_token=$data['google_access_token'];
                } else{
                    $user->linkedin_access_token=$data['linkedin_access_token'];
                }
                $user->save();
            }
            else{
                $user=User::create($data);
                if($register_type=='interviewee'){
                    Interviewee::create(['user_id' => $user['id']]);
                }
                else if($register_type=='interviewer'){
                    Interviewer::create(['user_id' => $user['id']]);
                }
                Session::forget('register_type');
            }
            Auth::login($user);
            $interviewee=Interviewee::where('user_id', $user['id'])->first();
            $interviewer=Interviewer::where('user_id', $user['id'])->first();
            if ($interviewee){
                Session::put('type', 'interviewee');
            }
            else if($interviewer){
                Session::put('type', 'interviewer');
                Session::forget('cart_ids');
            }
            if(!self::isMinimumProfileComplete()){
                return redirect(self::$minimumProfileCompletionRedirectPath);
            }
            return redirect('/');
        }
        return redirect()->back()->with('status', 'Something went wrong.Please try again.');
    }

    public function getMinimumCompleteProfile(Request $request){
        if(Auth::check()){
            return view('auth.complete_profile');
        }
        return redirect('login');
    }

    public function postMinimumCompleteProfile(Request $request){
        if(Auth::check() && !self::isMinimumProfileComplete()){
            $user = Auth::user();
            $d = explode('/', $request['dob']);
            $date = $d[2] . '-' . $d[1] . '-' . $d[0];
            $user->phone_no = $request['phone_number'];
            $user->city = $request['city'];
            $user->bio = $request['bio'];
            $user->dob = $date;
            $user->gender = $request['gender'];
            $user->save();
            return redirect('/');
            //return redirect('/dashboard');
        }
        return redirect('login');
    }

    public function saveAvailabilities(Request $request){
        //get user id
        $availability_post_data = Input::get('availability_post_data', -1);
        if ($availability_post_data != -1 && Auth::check()){
            $availability_post_data = explode(',', $availability_post_data);
            $interviewer_id = Interviewer::where('user_id', '=', Auth::user()->id)->first()->id;
            //Delele All availaibilities of the requester except appointments
            Availability::where('interviewer_id', '=', $interviewer_id)->whereNotIn('id', App\Appointment::all()->lists('availability_id')->toArray())->delete();
            //parse and store new ones
            $insert_data = array();
            foreach ($availability_post_data as $availability){
                $temp = explode('-', $availability);
                array_push($insert_data, array('start_timestamp' => Carbon::createFromTimestamp($temp[0]),
                    'end_timestamp' => Carbon::createFromTimestamp($temp[1]),
                    'interviewer_id' => $interviewer_id));
            }
            Availability::insert(array_values($insert_data));
        } else{
            Availability::where('end_timestamp', '<', Carbon::now())->whereNotIn('id', App\Appointment::all()->lists('availability_id')->toArray())->delete();
            return redirect('/');
        }
        return "OK";
    }
    
}