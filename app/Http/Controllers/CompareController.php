<?php

namespace App\Http\Controllers;

use App\Interviewer;
use App\User;
use Illuminate\Http\Request;
use Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CompareController extends Controller
{


    public function compare()
    {
        $data =[];
        $list = Interviewer::getDropList();
        $id1 = Input::get('id1');
        $id2 = Input::get('id2');
        if(($id2 != "" and $id1 == "") or ($id1 != "" and $id2 == ""))
        {
            if (($id2 != "" and $id1 == "")) {
            $id1 = $id2;
            $id2 = "";
            }
            $compareCase = 1;
            $user1 = Interviewer::find($id1)->user;
            $data=[];
            $interviewer = array($user1);
            $id = array($id1);
            $name = array(
                $user1['first_name']." ".$user1['middle_name']." ".$user1['last_name']);
            $email = array(
                $user1['email']);
            $phone = array(
                $user1['phone_no']);
            $city = array(
                $user1['city']);
            $bio = array(
                $user1['bio']);
            $resume_url = array(
                $user1['resume_url']);
            $profile_pic_url = array(
                $user1['profile_image_url']);
            $experience = array(
                User::find($user1['id'])->experience);
            $skills = array(
                User::find($user1['id'])->skills);
            $education = array(
                User::find($user1['id'])->education);
            $reviewsFor = array(
                User::find($user1['id'])->reviewsFor);
            $rating = array(
                Interviewer::find($id1)->avgRating());
            $yearsOfExp = array(
                Interviewer::find($id1)->yearsOfExperience()
            );


        }
        if($id1 != "" && $id2 != "")
        {
            $compareCase = 2;
            $user1 = Interviewer::find($id1)->user;
            $user2 = Interviewer::find($id2)->user;
            $data=[];
            $interviewer = array($user1, $user2);
            $id = array($id1,$id2);
            $name = array(
                $user1['first_name']." ".$user1['middle_name']." ".$user1['last_name'],
                $user2['first_name']." ".$user2['middle_name']." ".$user2['last_name']
            );
            $email = array(
                $user1['email'],
                $user2['email']
            );
            $phone = array(
                $user1['phone_no'],
                $user2['phone_no']
            );
            $city = array(
                $user1['city'],
                $user2['city']
            );
            $bio = array(
                $user1['bio'],
                $user2['bio']
            );
            $resume_url = array(
                $user1['resume_url'],
                $user2['resume_url']
            );
            $profile_pic_url = array(
                $user1['profile_image_url'],
                $user2['profile_image_url']
            );
            $experience = array(
                User::find($user1['id'])->experience,
                User::find($user2['id'])->experience
            );
            $skills = array(
                User::find($user1['id'])->skills,
                User::find($user2['id'])->skills
            );
            $education = array(
                User::find($user1['id'])->education->sortByDesc('start_date'),
                User::find($user2['id'])->education->sortByDesc('start_date')
            );
            $reviewsFor = array(
                User::find($user1['id'])->reviewsFor,
                User::find($user2['id'])->reviewsFor
            );

            $rating = array(
                Interviewer::find($id1)->avgRating(),
                Interviewer::find($id2)->avgRating()
            );
            $yearsOfExp = array(
                Interviewer::find($id1)->yearsOfExperience(),
                Interviewer::find($id2)->yearsOfExperience()

            );

        }
        if($id1 == "" && $id2 == "")
        {
            $compareCase = 0;
        }
    if($compareCase > 0) {
        $data['id'] = $id;
        $data['name'] = $name;
        $data['email'] = $email;
        $data['phone'] = $phone;
        $data['city'] = $city;
        $data['bio'] = $bio;
        $data['resume_url'] = $resume_url;
        $data['profile_image_url'] = $profile_pic_url;
        $data['experience'] = $experience;
        $data['skills'] = $skills;
        $data['education'] = $education;
        $data['reviewsFor'] = $reviewsFor;
        $data['rating'] = $rating;
        $data['yearsOfExp'] = $yearsOfExp;

        $data['interviewer'] = $interviewer;
        $reviewInterviewee=[];
        for($k = 0; $k < $compareCase; $k++)
        {
            foreach($reviewsFor[$k] as $review)
            {
                $reviewID = $review['id'];
                $ReviewByUserID = $review['by_user_id'];
                $IntervieweeRecord = \App\Interviewee::where('user_id',$ReviewByUserID)->first();
                $IntId = $IntervieweeRecord['id'];
                $reviewInterviewee[$reviewID] = $IntId;
            }
        }
        $data['reviewInt'] = $reviewInterviewee;
    }
        $data['compareCase'] = $compareCase;
        $data['list'] = $list;

        //dd($data);
        return view('compare', $data);
    }
}
