<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests;
use Log;
use Carbon\Carbon;

class FilterController extends Controller
{

    public function filterPrice($startPrice, $endPrice, $result){
        $c = new Collection();
        if($result==null){
            foreach (Listing::getOnlyAvailableListings() as $listing) {
                if($listing->price<=$endPrice && $listing->price>=$startPrice){
                    $c->add($listing);
                }
            }
        }
        else{
            foreach ($result as $listing) {
                if($listing->price<=$endPrice && $listing->price>=$startPrice){
                    $c->add($listing);
                }
            }
        }
        return $c;
    }

    public function filterCat($categories, $result){
        $c = new Collection();
        if($result==null){
            foreach (Listing::getOnlyAvailableListings() as $listing) {
                if(in_array($listing->subcategory->name,$categories)){
                    $c->add($listing);

                }
            }
        }
        else{
            foreach ($result as $listing) {
                if(in_array($listing->subcategory->name,$categories)){
                    $c->add($listing);
                }
            }
        }
        return $c;
    }

    public function filterCompany($companies, $result){
        $c = new Collection();
        if($result==null){
            foreach (Listing::getOnlyAvailableListings() as $listing) {
                foreach ($listing->interviewer->user->experience as $experience){
                    if(in_array($experience->company_name,$companies)){
                        $c->add($listing);
                        break;
                    }
                }
            }
        }
        else{
            foreach ($result as $listing) {
                foreach ($listing->interviewer->user->experience as $experience){
                    if(in_array($experience->company_name,$companies)){
                        $c->add($listing);
                        break;
                    }
                }
            }
        }
        return $c;
    }

    public function filterCert($result){
        $c = new Collection();
        if($result==null){
            foreach (Listing::getOnlyAvailableListings() as $listing) {
                if($listing->interviewer->verified==1){
                    $c->add($listing);
                }
            }
        }
        else{
            foreach ($result as $listing) {
                if($listing->interviewer->verified==1){
                    $c->add($listing);
                }
            }
        }
        return $c;
    }

    public function filterDate($start, $end, $result){
        $c = new Collection();
        $start = Carbon::createFromTimeStamp($start);
        $end = Carbon::createFromTimeStamp($end);
        if($result==null){
            foreach (Listing::getOnlyAvailableListings() as $listing) {
                foreach ($listing->interviewer->availabilities as $avail){
                    if($avail->start_timestamp>=$start && $avail->end_timestamp<=$end){
                        $c->add($listing);
                        break;
                    }
                }
            }
        }
        else{
            foreach ($result as $listing) {
                foreach ($listing->interviewer->availabilities as $avail){
                    if($avail->start_timestamp>=$start && $avail->end_timestamp<=$end){
                        $c->add($listing);
                        break;
                    }
                }
            }
        }
        return $c;
    }

    public function filterYears($years, $result){
        $c = new Collection();
        $years = intval($years*12);
        if($result==null){
            foreach (Listing::getOnlyAvailableListings() as $listing) {
                $expYears = $listing->interviewer->yearsOfExperience();
                Log::error($years." ".$expYears);
                if($expYears>=$years){
                    $c->add($listing);
                }
            }
        }
        else{
            foreach ($result as $listing) {
                $expYears = 0;
                foreach($listing->interviewer->user->experience as $exp){
                    $interval = $exp->end_date->diff($exp->start_date);
                    $expYears+=$interval->m+($interval->y*12);
                }
                if($expYears>=$years){
                    $c->add($listing);
                }
            }
        }
        return $c;
    }


    public function filterRating($rate, $result){
        $c = new Collection();
        if($result==null){
            foreach (Listing::getOnlyAvailableListings() as $listing) {
                $rating = $listing->interviewer->avgRating();
                if($rate<=$rating){
                    $c->add($listing);
                }
            }
        }
        else{
            foreach ($result as $listing) {
                $rating = $listing->interviewer->avgRating();
                if($rate<=$rating){
                    $c->add($listing);
                }
            }
        }
        return $c;
    }

    public function paginate($page, $limit0, $limit1 , $result){
        $c = new Collection();
        if($page==0){
            if($result->count()<$limit0) return $result;
            else{
                for($i = 0 ; $i < $limit0 ; $i++){
                    $c->add($result->get($i));
                }
                return $c;
            }
        }
        else{
            for($i=$limit0+(($page-1)*$limit1); $i<$limit0+(($page-1)*$limit1)+$limit1; $i++){
                if($result->get($i)!=null){
                    $c->add($result->get($i));
                }
            }
            return $c;
        }

    }

    public function getResult($query){
        $c = new Collection();
        //firstname lastname category subcategory companyname city description
        foreach (Listing::getOnlyAvailableListings() as $listing) {
            if(stripos($listing->interviewer->user->first_name,$query) !== false || stripos($query,$listing->interviewer->user->first_name) !== false){
                $c->add($listing);
            }
            else if(stripos($listing->interviewer->user->last_name,$query) !== false || stripos($query, $listing->interviewer->user->last_name) !== false){
                $c->add($listing);
            }
            else if(stripos($listing->subcategory->category->name,$query) !== false || stripos($query, $listing->subcategory->category->name) !== false){
                $c->add($listing);
            }
            else if(stripos($listing->subcategory->name,$query) !== false || stripos($query, $listing->subcategory->name) !== false){
                $c->add($listing);
            }
            else if(stripos($listing->interviewer->user->city,$query) || stripos($query, $listing->interviewer->user->city) !== false){
                $c->add($listing);
            }
            else if(stripos($listing->description,$query) !== false){
                $c->add($listing);
            }
            else {
                foreach ($listing->interviewer->user->experience as $experience){
                    if(stripos($experience->company_name,$query) !== false || stripos($query,$experience->company_name) !== false){
                        $c->add($listing);
                        break;
                    }
                }
            }
        }
        return $c;
    }
}