<?php
/**
 * Created by PhpStorm.
 * User: chirag
 * Date: 14/03/16
 * Time: 11:01
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Listing;
use App\Coupon;
use App\Coupon_Interviewee;
use \Session;
use Auth;

class CheckoutController extends Controller{
    //TODO
    //make payments table
    //make refunds table

    public function startCheckoutFlow(Request $request){

        //check login, redirect to login if needed
        if(Auth::check()) {
            $listing_id = Input::get('listing_id', -1);
            $cart_ids = Session::get('cart_ids', -1);
            if($listing_id != -1 && $cart_ids != -1 && in_array($listing_id,$cart_ids)) {

                $listing = Listing::where('id', '=', $listing_id)->get()->first();
                $all_coupons = Coupon::whereNull('listing_id')
                                     ->orWhere('listing_id', '=', $listing_id)
                                     ->where('count_left', '>', 0)
                                     ->get();
                $all_coupons_ids = array();
                foreach($all_coupons as $coupon){
                    array_push($all_coupons_ids, $coupon->id);
                }
                $all_coupon_interviewees = Coupon_Interviewee::whereIn('coupon_id',$all_coupons_ids)
                                                             ->where('interviewee_id','!=',Auth::user('id'))
                                                             ->orWhereNull('interviewee_id')
                                                             ->get();
                $all_coupon_interviewees_ids = array();
                foreach($all_coupon_interviewees as $coupon_interviewee){
                    array_push($all_coupon_interviewees_ids, $coupon_interviewee->id);
                }
                $coupons = array_diff($all_coupons_ids, $all_coupon_interviewees_ids);
                return view('payment.checkout', compact('listing','coupons'));
            }
            else {
                return redirect()->back();
            }
        }
        else {
            return redirect('login?redirect=shortlist');
        }

        //returns the page to select a slot and apply coupon and confirm

    }

    public function ccavenueRequestHandler(Request $request){
        //block the slot for the listing for some time?
        //mark coupon as floating consumed
        //send the discounted price

    }

    public function ccavenueResponseHandler(Request $request){
        $merchant_id = '';
        $access_code = '';
        $working_key = '';
        //consume the coupon
        //if (negative) -> back pressed or tab/browser closed or cancel button pressed
        //free the slot and not-consume the coupon
        //return a page with "NO"
        //else (positive)
        //consume coupon and slot
        //remove from cart
        //send email
        //set cronjob for the day of interview
        //return a page with "YES"
    }

    public function applyCouponOnListing(Request $request){

    }

}