<?php

/*
 * Describe you custom columns and form items here.
 *
 * There is some simple examples what you can use:
 *
 *		Column::register('customColumn', '\Foo\Bar\MyCustomColumn');
 *
 * 		FormItem::register('customElement', \Foo\Bar\MyCustomElement::class);
 *
 * 		FormItem::register('otherCustomElement', function (\Eloquent $model)
 * 		{
 *			AssetManager::addStyle(URL::asset('css/style-to-include-on-page-with-this-element.css'));
 *			AssetManager::addScript(URL::asset('js/script-to-include-on-page-with-this-element.js'));
 * 			if ($model->exists)
 * 			{
 * 				return 'My edit code.';
 * 			}
 * 			return 'My custom element code';
 * 		});
 */


Admin::model('\App\Category')->title('Category')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('name', 'Name');})

    ->form(function ()
    {
        FormItem::text('name', 'Name');
    });

Admin::model('\App\SubCategory')->title('Sub Category')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('name', 'Name');
        Column::string('category.name', 'Category');})

    ->form(function ()
    {
        FormItem::select('category_id', 'Category')->list(\App\Category::class);
        FormItem::text('name', 'Name');
        FormItem::multiSelect('skilledUsers', 'Skilled User')->list(\App\User::class)->value('skilledUsers.user_id');
    });

Admin::model('\App\User')->title('Users')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('first_name', 'First Name');
        Column::string('last_name', 'Last Name');
        Column::string('email', 'email');})
    ->form(function ()
    {
        FormItem::text('first_name', 'First Name');
        FormItem::text('middle_name', 'Middle Name');
        FormItem::text('last_name', 'Last Name');
        FormItem::text('email', 'email');
        FormItem::text('phone_no', 'Phone');
        FormItem::text('password', 'Password');//change to password
        FormItem::text('city', 'City');
        FormItem::textarea('bio', 'Bio');
        FormItem::date('dob', 'DOB');
        FormItem::file('resume_url', 'Resume')->validationRule('mimes:doc,docx,pdf,rtf');
        FormItem::file('profile_image_url', 'Profile Image')->validationRule('mimes:jpeg,bmp,png,jpg');
        FormItem::select('activated', 'Activated')->list(['0','1']);
        FormItem::multiSelect('skills', 'Skills for User')->list(\App\SubCategory::class)->value('skills.subcategory_id');
        FormItem::text('google_access_token', 'Google Access Token');
        FormItem::text('linkedin_access_token', 'LinkedIn Access Token');
        FormItem::select('gender', 'Gender')->list(['M','F','O']);
    });


Admin::model('\App\Interviewer')->title('Interviewer')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('user.first_name', 'User');
        Column::string('verified', 'Verified');})
    ->form(function ()
    {
        FormItem::select('verified', 'Verified')->list(['0','1']);
        FormItem::select('user_id', 'User')->list(\App\User::class);
    });

Admin::model('\App\Interviewee')->title('Interviewee')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('user.first_name', 'User');})
    ->form(function ()
    {
        FormItem::select('user_id', 'User')->list(\App\User::class);
        FormItem::multiSelect('coupons', 'Available Coupons')->list(\App\Coupon::class)->value('coupons.coupon_id');
    });

Admin::model('\App\Listing')->title('Listing')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('subcategory.name', 'SubCategory');
        Column::string('interviewer.user.first_name', 'Interviewer');
        Column::string('price', 'Price');})

    ->form(function ()
    {
        FormItem::select('subcategory_id', 'SubCategory')->list(\App\SubCategory::class);
        FormItem::select('interviewer_id', 'Interviewer')->list(\App\Interviewer::class);
        FormItem::text('price', 'Price');
        FormItem::textarea('description', 'Description');
        FormItem::select('disabled','Disabled')->list(['0','1']);
    });

Admin::model('\App\Appointment')->title('Appointments')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('listing.interviewer.user.first_name', 'Interviewer');
        Column::string('interviewee.user.first_name', 'Interviewee');
        Column::string('listing.subcategory.name', 'Sub Category');
        Column::string('availability.start_timestamp', 'time');})
    ->form(function ()
    {
        FormItem::select('interviewee_id', 'Interviewee')->list(\App\Interviewee::class);
        FormItem::select('listing_id', 'Listing')->list(\App\Listing::class);
        FormItem::select('availability_id', 'Availabilty')->list(\App\Availability::class);
    });

Admin::model('\App\Availability')->title('Availabilities')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('interviewer.user.first_name', 'Interviewer');
        Column::string('start_timestamp', 'Start Time');
        Column::string('end_timestamp', 'End Time');})
    ->form(function ()
    {
        FormItem::select('interviewer_id', 'Interviewer')->list(\App\Interviewer::class);
        FormItem::timestamp('start_timestamp', 'Start Time');
        FormItem::timestamp('end_timestamp', 'End Time');
    });

Admin::model('\App\Experience')->title('Experiences')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('user.first_name', 'User');
        Column::string('company_name', 'Company');
        Column::string('title', 'Title');})
    ->form(function ()
    {
        FormItem::select('user_id', 'User')->list(\App\User::class);
        FormItem::text('company_name', 'Company');
        FormItem::text('title', 'Title');
        FormItem::text('location', 'Location');
        FormItem::textarea('description', 'Description');
        FormItem::date('start_date', 'Start Time');
        FormItem::date('end_date', 'End Time');
    });

Admin::model('\App\Education')->title('Educations')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('user.first_name', 'User');
        Column::string('institution', 'Institution');
        Column::string('degree', 'Degree');})
    ->form(function ()
    {
        FormItem::select('user_id', 'User')->list(\App\User::class);
        FormItem::text('institution', 'Institution');
        FormItem::text('degree', 'Degree');
        FormItem::date('start_date', 'Start Time');
        FormItem::date('end_date', 'End Time');
    });

Admin::model('\App\Review')->title('Reviews')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('forUser.first_name', 'For');
        Column::string('byUser.last_name', 'By');
        Column::string('rating', 'Rating');})
    ->form(function ()
    {
        FormItem::select('for_user_id', 'For User')->list(\App\User::class);
        FormItem::select('by_user_id', 'By User')->list(\App\User::class);
        FormItem::select('listing_id', 'Listing')->list(\App\Listing::class);
        FormItem::text('text', 'Text');
        FormItem::text('rating', 'Rating');
    });

Admin::model('\App\Coupon')->title('Coupons')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('code', 'Code');
        Column::string('listing.subcategory.name', 'For Sub Category');
        Column::string('listing.interviewer.user.first_name', 'By Interviewer');
        Column::string('count_left', 'Count Left');
        Column::string('discount_percent', 'Discount');
        Column::string('start', 'Start');
        Column::string('end', 'End');})
    ->form(function ()
    {
        FormItem::text('code', 'Coupon Code');
        FormItem::select('listing_id', 'Listing')->list(\App\Listing::class);
        FormItem::text('count_left', 'Remaining Count');
        FormItem::text('discount_percent', 'Discount in Percent');
        FormItem::timestamp('start', 'Start Time');
        FormItem::timestamp('end', 'End Time');
        FormItem::multiSelect('interviewees', 'Valid For')->list(\App\Interviewee::class)->value('interviewees.interviewee_id');
    });

Admin::model('\App\Order')->title('Orders')
    ->columns(function () {
        Column::string('id', 'ID');
        Column::string('appointment.interviewee.user.first_name', 'Interviewee');
        Column::string('appointment.availability.interviewer.user.first_name', 'Interviewer');
        Column::string('appointment.listing.subcategory.name', 'SubCategory');
        Column::string('appointment.availability.start_timestamp', 'time');
        })
    ->form(function ()
    {
        FormItem::select('appointment_id', 'Appointment')->list(\App\Appointment::class);
        FormItem::text('offer_type', 'Offer type');
        FormItem::select('coupon_id', 'Coupon')->list(\App\Coupon::class);
        FormItem::text('amount', 'Amount');
        FormItem::text('payment_id', 'Payment Id');
        FormItem::text('tracking_id', 'Tracking Id');
        FormItem::select('payment_mode', 'Payment Mode')->list(['Credit Card','Debit Card','Net Banking']);
        FormItem::text('bank_ref_no', 'Bank Reference Number');
        FormItem::text('card_number', 'Card Number');
        FormItem::text('discount_value', 'Discount');
    });
//['price','description','subcategory_id','interviewer_id'];
