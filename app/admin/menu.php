<?php

/*
 * Describe your menu here.
 *
 * There is some simple examples what you can use:
 *
 * 		Admin::menu()->url('/')->label('Start page')->icon('fa-dashboard')->uses('\AdminController@getIndex');
 * 		Admin::menu(User::class)->icon('fa-user');
 * 		Admin::menu()->label('Menu with subitems')->icon('fa-book')->items(function ()
 * 		{
 * 			Admin::menu(\Foo\Bar::class)->icon('fa-sitemap');
 * 			Admin::menu('\Foo\Baz')->label('Overwrite model title');
 * 			Admin::menu()->url('my-page')->label('My custom page')->uses('\MyController@getMyPage');
 * 		});
 */

Admin::menu()->url('/')->label('Start page')->icon('fa-dashboard')->uses('\App\Http\Controllers\AdminController@getIndex');
Admin::menu(\App\Category::class)->url('categories/')->label('Categories')->icon('fa-th-large');
Admin::menu(\App\SubCategory::class)->url('sub_categories/')->label('Sub Categories')->icon('fa-list-ol');
Admin::menu(\App\User::class)->url('users/')->label('Users')->icon('fa-user');
Admin::menu(\App\Interviewer::class)->url('interviewers/')->label('Interviewers')->icon('fa-male');
Admin::menu(\App\Interviewee::class)->url('interviewees/')->label('Interviewees')->icon('fa-child');
Admin::menu(\App\Listing::class)->url('listings/')->label('Listings')->icon('fa-list');
Admin::menu(\App\Appointment::class)->url('appointments/')->label('Appointments')->icon('fa-calendar');
Admin::menu(\App\Availability::class)->url('availabilities/')->label('Availabilities')->icon('fa-clock-o');
Admin::menu(\App\Experience::class)->url('experiences/')->label('Experiences')->icon('fa-suitcase');
Admin::menu(\App\Education::class)->url('education/')->label('Educations')->icon('fa-graduation-cap');
Admin::menu(\App\Review::class)->url('reviews/')->label('Reviews')->icon('fa-comments');
Admin::menu(\App\Coupon::class)->url('coupons/')->label('Coupons')->icon('fa-money');
Admin::menu(\App\Order::class)->url('orders/')->label('Orders')->icon('fa-money');