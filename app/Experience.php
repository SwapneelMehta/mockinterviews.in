<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class Experience extends SleepingOwlModel
{
    //
    protected $table = 'experiences';
    protected $fillable = ['user_id','company_name','title','location','description','start_date','end_date'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function getDates()
    {
        return array_merge(parent::getDates(), ['start_date','end_date']);
    }
}