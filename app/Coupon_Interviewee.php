<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon_Interviewee extends Model
{
    //
    protected $table = 'coupon_interviewee';
    protected $fillable = ['coupon_id','used','interviewee_id'];

    public function coupon(){
        return $this->belongsTo('App\Coupon','coupon_id');
    }

    public function interviewee(){
        return $this->belongsTo('App\Interviewee','interviewee_id');
    }

}
