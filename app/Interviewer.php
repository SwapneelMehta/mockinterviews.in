<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;
use Carbon\Carbon;
use Log;

class Interviewer extends SleepingOwlModel
{
    //
    protected $table = 'interviewers';
    protected $fillable = ['verified','user_id'];
    protected $with = ['user'];

    public function toArray(){
        $array = parent::toArray();
        $array['rating']=$this->avgRating();
        $array['yearsofexperience']=$this->yearsOfExperience();
        return $array;
    }
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function listings(){
        return $this->hasMany('App\Listing','interviewer_id');
    }

    public function appointments(){
        $c = new Collection();
        foreach($this->listings as $list){
            foreach($list->appointments as $app){
                $c->add($app);
            }
        }
        return $c;
    }

    public function availabilities(){
        return $this->hasMany('App\Availability','interviewer_id')->where('start_timestamp','>',Carbon::now());
    }

    public function availabilitiesNotAppointments(){
        $c = new Collection();
        foreach($this->appointments() as $appointment){
            $c->add($appointment->availability);
        }
        return $this->availabilities->diff($c);
    }

    public static function getList(){
        $data = array();
        foreach (Interviewer::all() as $i)
        {
            $data += array($i->id => $i->user->first_name.' '.$i->user->last_name);
        }
        return $data;

    }

    public function avgRating(){
        $rating = $this->user->reviewsFor()->avg('rating');
        if($rating)
            return $rating;
        else
            return 0;
    }

    public function yearsOfExperience(){
        $expYears = 0;
        foreach($this->user->experience as $exp){
            $interval = $exp->end_date->diff($exp->start_date);
            $expYears+=$interval->m+($interval->y*12);
            Log::error($interval->m);
        }
        return $expYears;
    }

    public static function getDropList(){
        $data = [];
        $j = 0;
        foreach (Interviewer::all() as $i)
        {
            $data[$j] = array('id'=>$i->id, 'name'=>$i->user->first_name.' '.$i->user->last_name);
            $j+=1;
        }
        return $data;

    }
}
