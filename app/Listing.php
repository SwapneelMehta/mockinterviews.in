<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;
use Carbon\Carbon;

class Listing extends SleepingOwlModel
{
    //
    protected $table = 'listings';
    protected $fillable = ['price','description','subcategory_id','interviewer_id','disabled'];
    protected $with = ['subcategory','interviewer'];

    public function subcategory(){
        return $this->belongsTo('App\SubCategory','subcategory_id');
    }

    public function interviewer(){
        return $this->belongsTo('App\Interviewer','interviewer_id');
    }

    public function appointments(){
        return $this->hasMany('App\Appointment','listing_id');
    }

    public function coupons(){
        return $this->hasMany('App\Coupons','listing_id');
    }

    public function reviews(){
        return $this->hasMany('App\Review','listing_id');
    }

    public static function getList(){
        $data = array();
        foreach (Listing::all() as $i)
        {
            $data += array($i->id => $i->subcategory->name.' by '.$i->interviewer->user->first_name);
        }
        return $data;

    }

    public static function getOnlyAvailableListings()
    {
        $c = new Collection();
        foreach(Interviewer::all() as $interviewer){
            if(count($interviewer->availabilitiesNotAppointments())>0){
                foreach($interviewer->listings as $listing){
                    if(!$listing->disabled){
                        $c->add($listing);
                    }
                }
            }
        }
        return $c;
    }
}
//
//Listing::where('disabled','=',false)
//    ->whereIn('interviewer_id',
//        Availability::where('start_timestamp','>',Carbon::now())
//            ->groupBy('interviewer_id')->lists('interviewer_id')->toArray())->get();