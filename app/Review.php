<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class Review extends SleepingOwlModel
{
    //
    protected $table = 'reviews';
    protected $fillable = ['rating','text','for_user_id','by_user_id','listing_id'];
    protected $with = ['byUser'];

    public function forUser(){
        return $this->belongsTo('App\User','for_user_id');
    }

    public function byUser(){
        return $this->belongsTo('App\User','by_user_id');
    }

    public function listing(){
        return $this->belongsTo('App\Listing','listing_id');
    }

}
